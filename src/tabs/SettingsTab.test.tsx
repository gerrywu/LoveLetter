import * as React from "react";
import { shallow } from "enzyme";
import { SettingsTab } from "./SettingsTab";
import { Provider } from "react-redux";
import { createStore } from "redux";
import { rootReducer } from "../GameEngine/reducers/rootReducer";

describe("SettingsTab", () => {
  it("Matches snapshot", () => {
    const store = createStore(rootReducer);
    const wrapper = shallow(
      <Provider store={store}>
        <SettingsTab clearHistory={jest.fn()} setNumberOfPlayers={jest.fn()} />
      </Provider>
    );
    expect(wrapper.html()).toMatchSnapshot();
  });
});
