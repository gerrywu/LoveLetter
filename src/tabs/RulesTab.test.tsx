import * as React from "react";
import { shallow } from "enzyme";
import { RulesTab } from "./RulesTab";

describe("RulesTab", () => {
  it("Matches snapshot", () => {
    const wrapper = shallow(<RulesTab />);

    expect(wrapper.html()).toMatchSnapshot();
  });
});
