import React from "react";
import { Grid, Typography } from "@material-ui/core";
import { RulesTable } from "../components/RulesTable/RulesTable";

export const RulesTab: React.FunctionComponent = () => (
  <Grid container={true}>
    <Typography variant="h5">Game Tab</Typography>
    <Typography variant="body1">
      In game tab, you are player 1 and will be playing with 3 other AI players.
      In each turn you will have two cards to pick from and play. The effect and
      details of each card can be found in the table below.
    </Typography>
    <br />

    <Typography variant="h5">End Goal</Typography>
    <br />
    <Typography variant="body1">
      Game ends if the deck is empty at the end of a turn. The player with the
      highest ranked person wins. In case of a tie, the player who discarded the
      highest total value of cards wins.
    </Typography>

    <Typography variant="body1">
      Game also ends if all players but one are out of the game, in which case
      the remaining player wins.
    </Typography>

    <Typography>
      You can get the rules book{" "}
      <a
        href="http://online.fliphtml5.com/mvgr/hyvg/#p=1"
        target="_blank"
        rel="noopener noreferrer"
      >
        here
      </a>
      .
    </Typography>

    <RulesTable />
  </Grid>
);
