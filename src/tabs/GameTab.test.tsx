import * as React from "react";
import { shallow } from "enzyme";
import { GameTab } from "./GameTab";
import { Provider } from "react-redux";
import { createStore } from "redux";
import { rootReducer } from "../GameEngine/reducers/rootReducer";

jest.mock("../components/GameBoard/GameBoard", () => {
  const mockGameBoard: React.FunctionComponent<{
    compact: boolean;
  }> = () => {
    return <span>GameBoard</span>;
  };

  return {
    GameBoard: mockGameBoard,
  };
});

jest.mock("../components/GameBoard/SelectAction/SelectAction", () => {
  const mockSelectAction: React.FunctionComponent<{
    readonly nextTurn: () => void;
    readonly restart: () => void;
  }> = () => {
    return <span>SelectAction</span>;
  };

  return {
    SelectAction: mockSelectAction,
  };
});

describe("GameTab", () => {
  it("Matches snapshot", () => {
    const store = createStore(rootReducer);
    const wrapper = shallow(
      <Provider store={store}>
        <GameTab
          restart={jest.fn()}
          nextTurn={jest.fn()}
          alert={jest.fn()}
          playCard={jest.fn()}
        />
      </Provider>
    );
    expect(wrapper.html()).toMatchSnapshot();
  });
});
