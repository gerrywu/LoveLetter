import React from "react";
import { makeStyles, createStyles } from "@material-ui/core";
import { GameBoard } from "../components/GameBoard/GameBoard";

const useStyles = makeStyles(() =>
  createStyles({
    root: {
      flexGrow: 1,
    },
  })
);

export const FullGameBoardTab: React.FunctionComponent = () => {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <GameBoard key={"fullGameBoard"} />
    </div>
  );
};
