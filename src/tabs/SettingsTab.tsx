import React from "react";
import { useSelector } from "react-redux";
import RefreshIcon from "@material-ui/icons/Refresh";
import Button from "@material-ui/core/Button/Button";
import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";
import {
  Divider,
  FormControl,
  InputLabel,
  List,
  ListItem,
  ListItemProps,
  ListItemText,
  MenuItem,
  Select,
} from "@material-ui/core";
import { SelectInputProps } from "@material-ui/core/Select/SelectInput";
import { AppState } from "../GameEngine/reducers/rootReducer";

function ListItemLink(props: ListItemProps<"a", { button?: true }>) {
  return <ListItem button component="a" {...props} />;
}

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    formControl: {
      margin: theme.spacing(1),
      minWidth: 120,
    },
    selectEmpty: {
      marginTop: theme.spacing(2),
    },
  })
);

interface ReduxState {
  readonly numberOfPlayers: number;
  readonly winRate: number;
  readonly totalGames: string;
}

interface SettingsProps {
  clearHistory(): void;
  setNumberOfPlayers(numberOfPlayers: number): void;
}

export const SettingsTab: React.FunctionComponent<SettingsProps> = (
  props: SettingsProps
) => {
  const { setNumberOfPlayers, clearHistory } = props;
  const classes = useStyles();

  const { numberOfPlayers, winRate, totalGames } = useSelector<
    AppState,
    ReduxState
  >(
    (state: AppState): ReduxState => ({
      numberOfPlayers: state.gameReducer.numberOfPlayers,
      winRate:
        state.historyReducer.totalGames === "0"
          ? 0
          : Number(state.historyReducer.totalWins) /
            Number(state.historyReducer.totalGames),
      totalGames: state.historyReducer.totalGames,
    })
  );

  const handleChange: SelectInputProps["onChange"] = ({
    target: { value },
  }) => {
    setNumberOfPlayers(value as number);
  };

  return (
    <React.Fragment>
      <List component="nav" aria-label="main mailbox folders">
        <ListItem>
          <FormControl className={classes.formControl}>
            <InputLabel>Players</InputLabel>
            <Select value={numberOfPlayers} onChange={handleChange}>
              <MenuItem value={2}>2</MenuItem>
              <MenuItem value={3}>3</MenuItem>
              <MenuItem value={4}>4</MenuItem>
            </Select>
          </FormControl>
        </ListItem>
        <ListItem>
          <ListItemText
            primary={`Win rate: ${(Math.round(winRate * 100) / 100) * 100}%`}
          />
        </ListItem>
        <ListItem>
          <ListItemText primary={`Total Games Played: ${totalGames}`} />
        </ListItem>
        <ListItem>
          <Button variant="contained" color="primary" onClick={clearHistory}>
            <RefreshIcon />
            Clear History
          </Button>
        </ListItem>
      </List>
      <Divider />
      <List component="nav" aria-label="secondary mailbox folders">
        <ListItemLink href="https://gitlab.com/gerrywu/LoveLetter">
          <ListItemText primary="GitLab Project Link" />
        </ListItemLink>
      </List>
    </React.Fragment>
  );
};
