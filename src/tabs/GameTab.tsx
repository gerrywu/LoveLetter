import React, { useState } from "react";
import Grid from "@material-ui/core/Grid/Grid";
import { makeStyles, createStyles } from "@material-ui/core";
import { PlayedCard } from "../interfaces/Game";
import { HintDialog } from "../components/HintDialog/HintDialog";
import { GameBoard } from "../components/GameBoard/GameBoard";
import { SelectAction } from "../components/GameBoard/SelectAction/SelectAction";
import { RemainingCardListDialog } from "../components/RemainingCardListDialog/RemainingCardListDialog";

const useStyles = makeStyles(() =>
  createStyles({
    root: {
      flexGrow: 1,
    },
  })
);

interface GameTabProps {
  restart(): void;
  nextTurn(): void;
  playCard(playedCard: PlayedCard): void;
  alert(message: string): void;
}

export const GameTab: React.FunctionComponent<GameTabProps> = ({
  restart,
  nextTurn,
  playCard,
}) => {
  const classes = useStyles();
  const [hintDialogOpen, setHintDialogOpen] = useState<boolean>(false);
  const [
    remainingCardListDialogOpen,
    setRemainingCardListDialogOpen,
  ] = useState<boolean>(false);
  return (
    <div className={classes.root}>
      <Grid container={true} spacing={2}>
        <Grid item xs={12}>
          <GameBoard
            restart={restart}
            compact={true}
            openRemainingCardListDialog={() =>
              setRemainingCardListDialogOpen(true)
            }
          />
        </Grid>
        <Grid item xs={12}>
          <SelectAction
            playCard={playCard}
            nextTurn={nextTurn}
            restart={restart}
            openHintDialog={() => {
              setHintDialogOpen(true);
            }}
          />
        </Grid>
      </Grid>
      <RemainingCardListDialog
        open={remainingCardListDialogOpen}
        onClose={() => {
          setRemainingCardListDialogOpen(false);
        }}
      />
      <HintDialog
        open={hintDialogOpen}
        onClose={() => {
          setHintDialogOpen(false);
        }}
      />
    </div>
  );
};
