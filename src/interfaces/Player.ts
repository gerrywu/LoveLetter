import { GameState, PlayedCard } from "./Game";

export interface PlayerInterface {
  getAction(gameState: GameState): PlayedCard;
}
