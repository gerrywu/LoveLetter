interface PlayAction {
  cardId: number;
  target?: number;
  guess?: number;
}

export type SelectActionState = Partial<PlayAction>;
export const defaultState = {
  cardId: undefined,
  target: undefined,
  guess: undefined,
};
