import { Dictionary, GameState } from "./Game";

export const cardEmojis = ["💂", "👀", "⚔", "🧹", "🤴", "👑", "👩", "❄️"];

export const cardRank: Dictionary<number> = {
  Guard: 1,
  Priest: 2,
  Baron: 3,
  Handmaid: 4,
  Prince: 5,
  King: 6,
  Countess: 7,
  Princess: 8,
};

export const cardNames: string[] = [
  "Guard",
  "Priest",
  "Baron",
  "Handmaid",
  "Prince",
  "King",
  "Countess",
  "Princess",
];

export const initialPlayerState = {
  id: 1,
  dead: false,
  protected: false,
  holdingCards: [],
  playedCards: [],
  seenCards: [],
};

export const initialState: Partial<GameState> = {
  currentPlayerId: 1,
  numberOfPlayers: 4,
  players: [],
  firstCard: -1,
  availableCards: {
    Guard: 5,
    Priest: 2,
    Baron: 2,
    Handmaid: 2,
    Prince: 2,
    King: 1,
    Countess: 1,
    Princess: 1,
  },
  gameEnds: {
    winner: undefined,
  },
};

export const getInitialState = (numberOfPlayers: number): GameState => {
  return {
    ...JSON.parse(JSON.stringify(initialState)),
    hintsByPlayerId: new Map(),
    numberOfPlayers,
  };
};

export const tooltips: string[] = [
  "Guess a card",
  "Look opponent hand",
  "Compare hands",
  "Protect yourself",
  "Discard a card",
  "Exchange hands",
  "Discard if you have Prince or King",
  "Loses once discarded",
];
