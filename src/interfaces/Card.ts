export class Card {
  public id!: number;
  public cardName!: string;
  public emoji!: string;
  public Card(id: number, cardName: string, emoji: string) {
    this.id = id;
    this.cardName = cardName;
    this.emoji = emoji;
  }
}
