export interface PlayedCard {
  cardId: number;
  target?: number;
  discarded?: boolean;
  guess?: number;
  effect?: string;
}

export interface SeenCard {
  cardId: number;
  playerId: number;
}

export interface Player {
  id: number;
  protected: boolean;
  dead: boolean;
  holdingCards: number[];
  seenCards: SeenCard[];
  playedCards: PlayedCard[];
}

export interface Dictionary<TValue> {
  [id: string]: TValue;
}

export enum Operator {
  larger = "larger",
  equal = "equal",
  smaller = "smaller",
  notEqual = "notEqual",
}

export interface Hint {
  cardId: number;
  operator: Operator;
}

// ========Redux==============

export interface GameState {
  readonly numberOfPlayers: number;
  readonly currentPlayerId: number;
  readonly firstCard: number;
  readonly players: ReadonlyArray<Player>;
  // This map should be updated every turn
  readonly hintsByPlayerId: Map<number, Hint[]>; // player id, hints (Guard, Guess, Baron, King, Countess?)
  readonly availableCards: Dictionary<number>;
  readonly gameEnds: {
    winner?: Player;
  };
}

export interface GameAction {
  type:
    | "RESTART"
    | "PLAY_CARD"
    | "DISCARD_CARD"
    | "NEXT_PLAYER"
    | "DRAW_CARD"
    | "SET_NUMBER_OF_PLAYERS";
  numberOfPlayers?: number;
  cardToPlay?: PlayedCard;
  card?: PlayedCard;
}

export interface HistoryState {
  readonly totalGames: string;
  readonly totalWins: string;
}

export interface HistoryAction {
  winnerId?: number;
  type: "UPDATE_HISTORY" | "CLEAR_HISTORY";
}
