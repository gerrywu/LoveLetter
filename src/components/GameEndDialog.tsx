import { Button, Dialog, DialogActions, DialogTitle } from "@material-ui/core";
import React from "react";

interface GameEndDialogProps {
  readonly dialogOpen: boolean;
  readonly winnerId: number;
  onClose(restart?: boolean): void;
}

export const GameEndDialog: React.FunctionComponent<GameEndDialogProps> = (
  props: GameEndDialogProps
) => {
  const { dialogOpen, onClose, winnerId } = props;

  return (
    <Dialog
      onClose={() => onClose()}
      aria-labelledby="simple-dialog-title"
      open={dialogOpen}
    >
      <DialogTitle id="simple-dialog-title">
        {winnerId === 1
          ? "🏆 You are the winner!"
          : `Player ${winnerId} is the winner.`}
      </DialogTitle>
      <DialogActions>
        <Button onClick={() => onClose(true)} color="primary">
          Restart
        </Button>
        <Button onClick={() => onClose()} color="primary" autoFocus>
          Dismiss
        </Button>
      </DialogActions>
    </Dialog>
  );
};
