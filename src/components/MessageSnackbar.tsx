import React from "react";
import Snackbar from "@material-ui/core/Snackbar";
import IconButton from "@material-ui/core/IconButton/IconButton";
import CloseIcon from "@material-ui/icons/Close";

interface MessageSnackbarProps {
  readonly open: boolean;
  readonly message: string;

  onClose(
    event: React.SyntheticEvent | React.MouseEvent,
    reason?: string
  ): void;
}

export const MessageSnackbar: React.FC<MessageSnackbarProps> = ({
  open,
  message,
  onClose,
}) => {
  return (
    <Snackbar
      anchorOrigin={{
        vertical: "bottom",
        horizontal: "left",
      }}
      open={open}
      autoHideDuration={3000}
      onClose={onClose}
      message={message}
      action={[
        <IconButton
          key="close"
          aria-label="Close"
          color="inherit"
          onClick={onClose}
        >
          <CloseIcon />
        </IconButton>,
      ]}
    />
  );
};
