import * as React from "react";
import { shallow } from "enzyme";
import { GameEndDialog } from "./GameEndDialog";

describe("GameEndDialog", () => {
  it("Matches snapshot", () => {
    const wrapper = shallow(
      <GameEndDialog winnerId={3} dialogOpen={true} onClose={jest.fn()} />
    );

    expect(wrapper.html()).toMatchSnapshot();
  });
});
