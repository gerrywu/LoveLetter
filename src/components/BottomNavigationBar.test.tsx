import * as React from "react";
import { shallow } from "enzyme";
import { BottomNavigationBar } from "./BottomNavigationBar";
import { Tab } from "./Page";

describe("BottomNavigationBar", () => {
  it("Matches snapshot", () => {
    const wrapper = shallow(
      <BottomNavigationBar tab={Tab.game} handleChange={jest.fn()} />
    );

    expect(wrapper.html()).toMatchSnapshot();
  });
});
