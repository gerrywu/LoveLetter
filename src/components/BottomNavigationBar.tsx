import {
  BottomNavigation,
  BottomNavigationAction,
  createStyles,
} from "@material-ui/core";
import SportsEsportsIcon from "@material-ui/icons/SportsEsports";
import DescriptionIcon from "@material-ui/icons/Description";
import MenuBookIcon from "@material-ui/icons/MenuBook";
import SettingsIcon from "@material-ui/icons/Settings";
import React from "react";
import makeStyles from "@material-ui/core/styles/makeStyles";
import { Tab } from "./Page";

const useStyles = makeStyles(() =>
  createStyles({
    stickToBottom: {
      width: "100%",
      position: "fixed",
      bottom: 0,
    },
  })
);

interface BottomNavigationBarProps {
  readonly tab: Tab;
  handleChange(event: React.ChangeEvent<any>, value: string): void;
}

export const BottomNavigationBar: React.FunctionComponent<BottomNavigationBarProps> = (
  props: BottomNavigationBarProps
) => {
  const { tab, handleChange } = props;
  const classes = useStyles();
  return (
    <BottomNavigation
      value={tab}
      onChange={handleChange}
      className={classes.stickToBottom}
    >
      <BottomNavigationAction
        label="Game"
        value="game"
        icon={<SportsEsportsIcon />}
      />
      <BottomNavigationAction
        label="Full Board"
        value="fullGameBoard"
        icon={<DescriptionIcon />}
      />
      <BottomNavigationAction
        label="Rules"
        value="rules"
        icon={<MenuBookIcon />}
      />
      <BottomNavigationAction
        label="Settings"
        value="settings"
        icon={<SettingsIcon />}
      />
    </BottomNavigation>
  );
};
