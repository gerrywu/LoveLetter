import * as React from "react";
import { shallow } from "enzyme";
import { RulesTable } from "./RulesTable";

describe("RulesTable", () => {
  it("Matches snapshot", () => {
    const wrapper = shallow(<RulesTable />);

    expect(wrapper.html()).toMatchSnapshot();
  });
});
