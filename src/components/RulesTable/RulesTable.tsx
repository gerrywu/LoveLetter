import React from "react";
import makeStyles from "@material-ui/core/styles/makeStyles";
import {
  TableBody,
  Table,
  TableCell,
  TableHead,
  TableRow,
  Paper,
  createStyles,
  Theme,
} from "@material-ui/core";
import { cardEmojis } from "../../interfaces/Constants";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      width: "100%",
      marginTop: theme.spacing(3),
      overflowX: "auto",
    },
    table: {
      minWidth: 700,
    },
  })
);

interface CharacterDescriptor {
  name: string;
  description: string;
  rank: number;
  amount: number;
  icon: React.ReactElement;
}

export const getCharacterIcon = (rank: number): React.ReactElement => {
  return (
    <span role="img" aria-label="sheep">
      {cardEmojis[rank - 1]}
    </span>
  );
};

const rows: CharacterDescriptor[] = [
  {
    name: "Guard",
    description:
      "Choose another player and guess his/her hand, the person is knocked out if you guessed right",
    rank: 1,
    amount: 5,
    icon: (
      <span role="img" aria-label="sheep">
        💂
      </span>
    ),
  },
  {
    name: "Priest",
    description: "Secretly look at another player's hand",
    rank: 2,
    amount: 2,
    icon: (
      <span role="img" aria-label="sheep">
        👀
      </span>
    ),
  },
  {
    name: "Baron",
    description:
      "Compare your other card with another player, the person with the lower rank card is knocked out",
    rank: 3,
    amount: 2,
    icon: (
      <span role="img" aria-label="sheep">
        ⚔️
      </span>
    ),
  },
  {
    name: "Handmaid",
    description: "Protect yourself from other players until next turn",
    rank: 4,
    amount: 2,
    icon: (
      <span role="img" aria-label="sheep">
        🧹
      </span>
    ),
  },
  {
    name: "Prince",
    description: "Choose one player to discards his or her hand",
    rank: 5,
    amount: 2,
    icon: (
      <span role="img" aria-label="sheep">
        🤴
      </span>
    ),
  },
  {
    name: "King",
    description: "Trade hands with another player",
    rank: 6,
    amount: 1,
    icon: (
      <span role="img" aria-label="sheep">
        👑
      </span>
    ),
  },
  {
    name: "Countess",
    description: "Discard if the other card you have is King or Prince",
    rank: 7,
    amount: 1,
    icon: (
      <span role="img" aria-label="sheep">
        👩
      </span>
    ),
  },
  {
    name: "Princess",
    description: "Knocked out if discarded",
    rank: 8,
    amount: 1,
    icon: (
      <span role="img" aria-label="sheep">
        ❄️
      </span>
    ),
  },
];

export const RulesTable: React.FunctionComponent = () => {
  const classes = useStyles();

  return (
    <Paper className={classes.root}>
      <Table className={classes.table}>
        <TableHead>
          <TableRow>
            <TableCell>Rank</TableCell>
            <TableCell>Icon</TableCell>
            <TableCell>Name</TableCell>
            <TableCell>Amount</TableCell>
            <TableCell>Description</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {rows.map((row: CharacterDescriptor, index: number) => (
            <TableRow key={index} hover>
              <TableCell component="th" scope="row">
                {row.rank}
              </TableCell>
              <TableCell>{row.icon}</TableCell>
              <TableCell>{row.name}</TableCell>
              <TableCell>{row.amount}</TableCell>
              <TableCell>{row.description}</TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </Paper>
  );
};
