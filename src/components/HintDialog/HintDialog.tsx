import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
} from "@material-ui/core";
import React from "react";
import { useSelector } from "react-redux";
import { HintContent } from "./HintContent";
import { Hint } from "../../interfaces/Game";
import { AppState } from "../../GameEngine/reducers/rootReducer";

interface ReduxState {
  readonly hintsByPlayerId: Map<number, Hint[]>;
  readonly currentPlayerId: number;
}

interface HintDialogProps {
  readonly open: boolean;
  onClose(): void;
}

export const HintDialog: React.FunctionComponent<HintDialogProps> = (
  props: HintDialogProps
) => {
  const { open, onClose } = props;
  const { hintsByPlayerId, currentPlayerId } = useSelector<
    AppState,
    ReduxState
  >((state) => {
    return {
      hintsByPlayerId: state.gameReducer.hintsByPlayerId,
      currentPlayerId: state.gameReducer.currentPlayerId,
    };
  });

  const hintsDisplay: React.ReactNode[] = [];
  hintsByPlayerId.forEach((hints: Hint[], playerId: number) => {
    hints.forEach((hint) => {
      if (playerId !== currentPlayerId) {
        hintsDisplay.push(
          <HintContent
            key={hintsDisplay.length}
            playerId={playerId}
            hint={hint}
          />
        );
      }
    });
  });

  return (
    <Dialog
      open={open}
      onClose={onClose}
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description"
    >
      <DialogTitle id="alert-dialog-title">Hints</DialogTitle>
      <DialogContent>
        <DialogContentText id="alert-dialog-description">
          {hintsDisplay}
        </DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button onClick={onClose} color="primary" autoFocus={true}>
          Close
        </Button>
      </DialogActions>
    </Dialog>
  );
};
