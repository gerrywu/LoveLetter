import { AppState } from "../../GameEngine/reducers/rootReducer";
import { Hint, Operator } from "../../interfaces/Game";

export const hintDialogMockState: AppState = {
  gameReducer: {
    currentPlayerId: 1,
    numberOfPlayers: 4,
    hintsByPlayerId: new Map<number, Hint[]>([
      [2, [{ cardId: 3, operator: Operator.notEqual }]],
      [3, [{ cardId: 5, operator: Operator.larger }]],
    ]),
    players: [
      {
        id: 1,
        dead: false,
        protected: false,
        holdingCards: [1, 2],
        playedCards: [],
        seenCards: [],
      },
      {
        id: 2,
        dead: false,
        protected: false,
        holdingCards: [4],
        playedCards: [],
        seenCards: [],
      },
      {
        id: 3,
        dead: false,
        protected: false,
        holdingCards: [2],
        playedCards: [],
        seenCards: [],
      },
      {
        id: 4,
        dead: false,
        protected: false,
        holdingCards: [8],
        playedCards: [],
        seenCards: [],
      },
    ],
    firstCard: -1,
    availableCards: {
      Guard: 5,
      Priest: 2,
      Baron: 2,
      Handmaid: 2,
      Prince: 2,
      King: 1,
      Countess: 1,
      Princess: 1,
    },
    gameEnds: {
      winner: undefined,
    },
  },
  historyReducer: {
    totalGames: "0",
    totalWins: "0",
  },
};
