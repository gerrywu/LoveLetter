import * as React from "react";
import { shallow } from "enzyme";
import { Provider } from "react-redux";
import { createStore } from "redux";
import { HintDialog } from "./HintDialog";
import { GameActionTypes } from "../../../GameEngine/actions/game";
import { hintDialogMockState } from "./HintDialog.mock";
import { AppState, rootReducer } from "../../GameEngine/reducers/rootReducer";

describe("SelectAction", () => {
  it("Matches snapshot", () => {
    const store = createStore<AppState, GameActionTypes, void, void>(
      rootReducer,
      hintDialogMockState
    );

    const wrapper = shallow(
      <Provider store={store}>
        <HintDialog open={true} onClose={jest.fn()} />
      </Provider>
    );
    expect(wrapper.html()).toMatchSnapshot();
  });
});
