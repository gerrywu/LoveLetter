import React from "react";
import { Hint, Operator } from "../../interfaces/Game";
import { cardNames } from "../../interfaces/Constants";

interface HintContentProps {
  readonly hint: Hint;
  readonly playerId: number;
}

export const HintContent: React.FunctionComponent<HintContentProps> = (
  props: HintContentProps
) => {
  const { hint, playerId } = props;
  let hintString = "";
  const cardName = cardNames[hint.cardId - 1];
  switch (hint.operator) {
    case Operator.equal: {
      hintString = `has ${cardName}`;
      break;
    }
    case Operator.notEqual: {
      hintString = `doesn't have ${cardName}`;
      break;
    }
    case Operator.larger: {
      hintString = `card is larger than ${cardName}`;
      break;
    }
    case Operator.smaller: {
      hintString = `card is smaller than ${cardName}`;
      break;
    }
  }
  return (
    <React.Fragment>
      Player {playerId}: {hintString} <br />
    </React.Fragment>
  );
};
