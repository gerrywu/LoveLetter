import * as React from "react";
import { shallow } from "enzyme";
import { Provider } from "react-redux";
import { createStore } from "redux";
import { GameBoard } from "./GameBoard";
import { rootReducer } from "../../GameEngine/reducers/rootReducer";

describe("GameBoard", () => {
  it("Matches snapshot", () => {
    const store = createStore(rootReducer);
    const wrapper = shallow(
      <Provider store={store}>
        <GameBoard />
      </Provider>
    );
    expect(wrapper.html()).toMatchSnapshot();
  });
});
