import React from "react";
import { useSelector } from "react-redux";
import {
  Button,
  Card,
  CardContent,
  CardHeader,
  Divider,
  IconButton,
} from "@material-ui/core";
import HelpOutlineIcon from "@material-ui/icons/HelpOutline";
import { PlayedCardList } from "./PlayedCardList";
import makeStyles from "@material-ui/core/styles/makeStyles";
import { createStyles, Theme } from "@material-ui/core";
import { Dictionary, Player } from "../../interfaces/Game";
import { getAvailableCardSize } from "../../GameEngine/util";
import { AppState } from "../../GameEngine/reducers/rootReducer";
import RefreshIcon from "@material-ui/icons/Refresh";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    cardHeader: {
      backgroundColor: theme.palette.grey[200],
    },
    cardSubHeader: {
      display: "flex",
    },
    cardContent: {
      padding: 0,
    },
    openRemainingDialogIconButton: {
      marginLeft: "auto",
      padding: 0,
    },
  })
);

interface ReduxState {
  readonly players: ReadonlyArray<Player>;
  readonly winner?: Player;
  readonly currentPlayerId: number;
  readonly firstCard: number;
  readonly availableCards: Dictionary<number>;
}

interface GameBoardProps {
  readonly compact?: boolean;
  restart?(): void;
  openRemainingCardListDialog?(): void;
}

export const GameBoard: React.FC<GameBoardProps> = ({
  compact,
  restart,
  openRemainingCardListDialog,
}) => {
  const classes = useStyles();
  const { players, winner, currentPlayerId, availableCards } = useSelector<
    AppState,
    ReduxState
  >((state) => ({
    currentPlayerId: state.gameReducer.currentPlayerId,
    players: state.gameReducer.players,
    winner: state.gameReducer.gameEnds.winner,
    firstCard: state.gameReducer.firstCard,
    availableCards: state.gameReducer.availableCards,
  }));

  const cardsLeft = getAvailableCardSize(availableCards);

  const playedCardLists = players.map((player, index) => (
    <div key={index}>
      {index !== 0 && <Divider variant="middle" />}
      <PlayedCardList
        currentPlayerId={currentPlayerId}
        player={player}
        key={index}
        winner={winner}
        compact={compact}
      />
    </div>
  ));

  return (
    <Card>
      <CardHeader
        subheader={
          <div className={classes.cardSubHeader}>
            Available cards: {cardsLeft}
            <Button
              style={{ marginLeft: "auto" }}
              color="default"
              variant="outlined"
              onClick={restart}
              size="small"
            >
              <RefreshIcon />
              Restart
            </Button>
            <IconButton
              aria-label={"openRemainingDialogIconButton"}
              size={"small"}
              className={classes.openRemainingDialogIconButton}
              onClick={() => {
                if (openRemainingCardListDialog) {
                  openRemainingCardListDialog();
                }
              }}
            >
              <HelpOutlineIcon />
            </IconButton>
          </div>
        }
        className={classes.cardHeader}
        titleTypographyProps={{
          variant: "h6",
        }}
      />
      <CardContent className={classes.cardContent}>
        {playedCardLists}
      </CardContent>
    </Card>
  );
};
