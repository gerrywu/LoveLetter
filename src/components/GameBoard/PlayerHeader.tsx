import { Player } from "../../interfaces/Game";
import React from "react";
import { CircularProgress, ListSubheader } from "@material-ui/core";
import { cardEmojis } from "../../interfaces/Constants";

const getPlayerStatus = (player: Player, winner?: Player): string => {
  let playerStatusString = "";
  if (winner?.id) {
    if (winner.id === player.id) {
      playerStatusString += ` - 🏆`;
      if (player.holdingCards[0] !== undefined) {
        playerStatusString += ` - last card: ${
          cardEmojis[player.holdingCards[0] - 1]
        }`;
      }
    } else {
      playerStatusString += ` - 😡`;
      if (player.holdingCards[0] !== undefined) {
        playerStatusString += ` - last card: ${
          cardEmojis[player.holdingCards[0] - 1]
        }`;
      }
    }
  } else {
    if (player.dead) {
      playerStatusString += ` - ⚰️`;
      if (player.holdingCards[0] !== undefined) {
        playerStatusString += ` - last card: ${
          cardEmojis[player.holdingCards[0] - 1]
        }`;
      }
    } else if (player.protected) {
      playerStatusString += " - ❤️";
    }
  }
  return playerStatusString;
};

interface PlayerHeaderProps {
  readonly player: Player;
  readonly winner?: Player;
  readonly currentPlayerId: number;
}

export const PlayerHeader: React.FC<PlayerHeaderProps> = ({
  player,
  currentPlayerId,
  winner,
}) => {
  const activePlayer =
    currentPlayerId === player.id && !player.dead && winner === undefined;
  const showLoading = activePlayer && currentPlayerId === 1;

  return (
    <ListSubheader
      component="div"
      style={{
        backgroundColor: activePlayer
          ? "#bbdefb"
          : player.dead
          ? "#ffcdd2"
          : undefined,
        transition: "background-color 0.5s",
        borderRadius: 10,
        margin: 8,
        lineHeight: "36px",
      }}
      color={
        player.dead
          ? "default"
          : currentPlayerId === player.id
          ? "primary"
          : "default"
      }
    >
      {`Player ${player.id}${getPlayerStatus(player, winner)} `}
      {showLoading && (
        <CircularProgress
          size={20}
          style={{ right: 10, position: "absolute", top: "30%" }}
        />
      )}
    </ListSubheader>
  );
};
