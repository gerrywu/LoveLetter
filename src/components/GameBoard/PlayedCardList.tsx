import React from "react";
import { List } from "@material-ui/core";
import { Player } from "../../interfaces/Game";
import { PlayerHeader } from "./PlayerHeader";
import { PlayerCard } from "./PlayerCard";

interface PlayedCardListProps {
  readonly player: Player;
  readonly winner?: Player;
  readonly compact?: boolean;
  readonly currentPlayerId: number;
}

export const PlayedCardList: React.FunctionComponent<PlayedCardListProps> = ({
  player,
  winner,
  compact,
  currentPlayerId,
}) => (
  <List
    dense={false}
    subheader={
      <PlayerHeader
        currentPlayerId={currentPlayerId}
        winner={winner}
        player={player}
      />
    }
  >
    <PlayerCard player={player} compact={compact} />
  </List>
);
