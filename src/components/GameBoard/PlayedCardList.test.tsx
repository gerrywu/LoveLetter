import * as React from "react";
import { shallow } from "enzyme";
import { PlayedCardList } from "./PlayedCardList";
import { Player } from "../../../interfaces/Game";

describe("PlayedCardList", () => {
  it("Matches snapshot", () => {
    const player: Player = {
      id: 1,
      holdingCards: [1, 2],
      playedCards: [],
      seenCards: [],
      dead: false,
      protected: false,
    };
    const wrapper = shallow(
      <PlayedCardList
        currentPlayerId={1}
        player={player}
        winner={undefined}
        compact={false}
      />
    );
    expect(wrapper.html()).toMatchSnapshot();
  });
});
