import * as React from "react";
import { shallow } from "enzyme";
import { Provider } from "react-redux";
import { createStore } from "redux";
import { SelectAction } from "./SelectAction";
import { GameActionTypes } from "../../../../GameEngine/actions/game";
import {
  AppState,
  rootReducer,
} from "../../../GameEngine/reducers/rootReducer";

describe("SelectAction", () => {
  it("Matches snapshot", () => {
    const store = createStore<AppState, GameActionTypes, void, void>(
      rootReducer,
      {
        // @ts-ignore
        gameReducer: {
          currentPlayerId: 1,
          numberOfPlayers: 4,
          hintsByPlayerId: new Map(),
          players: [
            {
              id: 1,
              dead: false,
              protected: false,
              holdingCards: [1, 2],
              playedCards: [],
              seenCards: [],
            },
            {
              id: 2,
              dead: false,
              protected: false,
              holdingCards: [4],
              playedCards: [],
              seenCards: [],
            },
            {
              id: 3,
              dead: false,
              protected: false,
              holdingCards: [2],
              playedCards: [],
              seenCards: [],
            },
            {
              id: 4,
              dead: false,
              protected: false,
              holdingCards: [8],
              playedCards: [],
              seenCards: [],
            },
          ],
          firstCard: -1,
          availableCards: {
            Guard: 5,
            Priest: 2,
            Baron: 2,
            Handmaid: 2,
            Prince: 2,
            King: 1,
            Countess: 1,
            Princess: 1,
          },
          gameEnds: {
            winner: undefined,
          },
        },
        historyReducer: {
          totalGames: "0",
          totalWins: "0",
        },
      }
    );

    const wrapper = shallow(
      <Provider store={store}>
        <SelectAction
          playCard={jest.fn()}
          restart={jest.fn()}
          nextTurn={jest.fn()}
          openHintDialog={jest.fn()}
        />
      </Provider>
    );
    expect(wrapper.html()).toMatchSnapshot();
  });
});
