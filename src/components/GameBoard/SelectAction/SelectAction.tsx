import React, { useState } from "react";
import { useSelector } from "react-redux";
import makeStyles from "@material-ui/core/styles/makeStyles";
import {
  Button,
  Card,
  CardActions,
  CardContent,
  CardHeader,
  createStyles,
  IconButton,
  Theme,
} from "@material-ui/core";
import { ChooseCard } from "./GameSteps/ChooseCard";
import { ChooseTarget } from "./GameSteps/ChooseTarget";
import { ChooseGuess } from "./GameSteps/ChooseGuess";
import { StepTitle } from "./StepTitle";
import InfoOutlinedIcon from "@material-ui/icons/InfoOutlined";
import NavigateNextIcon from "@material-ui/icons/NavigateNext";
import { PlayedCard, Player } from "../../../interfaces/Game";
import {
  defaultState,
  SelectActionState,
} from "../../../interfaces/SelectAction";
import { getRemainingCardCountsFromPlayers } from "../../../GameEngine/util";
import { AppState } from "../../../GameEngine/reducers/rootReducer";

export const getActiveStep = (state: SelectActionState) => {
  let activeStep = 0;
  if (state.cardId !== undefined) {
    activeStep = 1;
  }
  if (state.target !== undefined) {
    activeStep = 2;
  }
  return activeStep;
};

const noTarget = (playerTargetable: boolean[]) =>
  playerTargetable.every((player) => !player);

interface ReduxState {
  readonly numberOfPlayers: number;
  readonly isPlayersTurn: boolean;
  readonly holdingCards: number[];
  readonly remainingCardCounts: number[];
  readonly playerTargetable: boolean[];
  readonly hintsDisabled: boolean;
  readonly gameEnds: boolean;
}

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    cardHeader: {
      backgroundColor: theme.palette.grey[200],
    },
    cardContent: {
      justifyContent: "space-evenly",
      padding: 0,
      minHeight: 55,
      display: "flex",
    },
  })
);

interface SelectActionProps {
  restart(): void;
  nextTurn(): void;
  playCard(playedCard: PlayedCard): void;
  openHintDialog(): void;
}

export const SelectAction: React.FunctionComponent<SelectActionProps> = ({
  nextTurn,
  playCard,
  openHintDialog,
}) => {
  const classes = useStyles();
  const [state, setState] = useState<SelectActionState>(defaultState);
  const {
    numberOfPlayers,
    isPlayersTurn,
    holdingCards,
    remainingCardCounts,
    playerTargetable,
    hintsDisabled,
  } = useSelector<AppState, ReduxState>(
    (state): ReduxState => {
      return {
        gameEnds: state.gameReducer.gameEnds.winner !== undefined,
        hintsDisabled:
          state.gameReducer.currentPlayerId !==
            state.gameReducer.players[0].id ||
          state.gameReducer.hintsByPlayerId.size === 0,
        numberOfPlayers: state.gameReducer.numberOfPlayers,
        remainingCardCounts: getRemainingCardCountsFromPlayers(
          state.gameReducer.players
        ),
        holdingCards: state.gameReducer.players[0].holdingCards,
        isPlayersTurn:
          state.gameReducer.gameEnds.winner === undefined &&
          !state.gameReducer.players[0].dead &&
          state.gameReducer.currentPlayerId === state.gameReducer.players[0].id,
        playerTargetable: state.gameReducer.players.map(
          (player: Player) =>
            !player.dead && !player.protected && player.id !== 1
        ),
      };
    }
  );

  const submitAction = (action: SelectActionState) => {
    if (action.cardId !== undefined) {
      playCard({
        ...action,
        cardId: action.cardId,
      });

      setState(defaultState);
      nextTurn();
    }
  };

  const chooseCard = (cardId: number) => {
    console.assert(
      holdingCards.includes(cardId),
      `${cardId} not in ${holdingCards}]`
    );
    if ([4, 7, 8].includes(cardId)) {
      submitAction({ ...state, cardId });
    } else if (cardId !== 5 && noTarget(playerTargetable)) {
      submitAction({ ...state, cardId });
    } else {
      setState({ ...state, cardId });
    }
  };

  const chooseTarget = (target: number) => {
    if (state.cardId !== 1) {
      submitAction({ ...state, target });
    } else {
      setState({ ...state, target });
    }
  };

  const chooseGuess = (guess: number) => {
    submitAction({ ...state, guess });
  };

  const handleBack = () => {
    const activeStep = getActiveStep(state);
    if (activeStep === 1) {
      setState({
        ...state,
        cardId: undefined,
      });
    } else if (activeStep === 2) {
      setState({
        ...state,
        target: undefined,
      });
    }
  };

  const renderChooseCard = () => {
    let countessAndPrinceOrKingPresent = false;
    if (holdingCards.includes(7)) {
      if (holdingCards.includes(5) || holdingCards.includes(6)) {
        countessAndPrinceOrKingPresent = true;
      }
    }

    return holdingCards
      .sort()
      .map((holdingCard, index) => (
        <ChooseCard
          holdingCard={holdingCard}
          isPlayersTurn={isPlayersTurn}
          chooseCard={chooseCard}
          disabled={countessAndPrinceOrKingPresent && holdingCard !== 7}
          key={index}
        />
      ));
  };

  const renderChooseTarget = () => {
    const targetPlayerArray = [];
    for (let i = 0; i < numberOfPlayers; i += 1) {
      targetPlayerArray.push(i + 1);
    }

    return targetPlayerArray.map((target, index) => (
      <ChooseTarget
        key={index}
        target={target}
        chooseTarget={chooseTarget}
        playerTargetable={
          index === 0 ? state.cardId === 5 : playerTargetable[index]
        }
      />
    ));
  };

  const renderChooseGuess = () => {
    return [1, 2, 3, 4, 5, 6, 7, 8].map((guess, index) => (
      <ChooseGuess
        key={index}
        playedCards={remainingCardCounts}
        guess={guess}
        chooseGuess={chooseGuess}
        disabled={guess === 1}
      />
    ));
  };

  let display;

  const activeStep = getActiveStep(state);
  switch (activeStep) {
    case 0:
      display = renderChooseCard();
      break;
    case 1:
      display = renderChooseTarget();
      break;
    case 2:
      display = renderChooseGuess();
      break;
    default:
      display = null;
      break;
  }

  return (
    <Card>
      <CardHeader
        titleTypographyProps={{
          variant: "h6",
        }}
        subheader={<StepTitle state={state} />}
        className={classes.cardHeader}
      />
      <CardContent className={classes.cardContent}>
        {display}
        {activeStep !== 2 && (
          <IconButton
            aria-label="delete"
            disabled={hintsDisabled}
            style={{ marginLeft: "auto" }}
            onClick={openHintDialog}
          >
            <InfoOutlinedIcon />
          </IconButton>
        )}
      </CardContent>
      <CardActions>
        <Button disabled={activeStep === 0} onClick={handleBack}>
          Back
        </Button>
        <Button
          style={{ marginLeft: "auto" }}
          color="default"
          variant="outlined"
          onClick={nextTurn}
          disabled={isPlayersTurn}
          size="small"
        >
          <NavigateNextIcon />
          Next Turn
        </Button>
      </CardActions>
    </Card>
  );
};
