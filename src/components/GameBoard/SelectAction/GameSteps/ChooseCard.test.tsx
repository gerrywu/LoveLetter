import * as React from "react";
import { shallow } from "enzyme";
import { ChooseCard } from "./ChooseCard";

describe("ChooseCard", () => {
  it("Matches snapshot", () => {
    const wrapper = shallow(
      <ChooseCard holdingCard={1} isPlayersTurn={true} chooseCard={jest.fn()} />
    );

    expect(wrapper.html()).toMatchSnapshot();
  });
});
