import {
  Badge,
  makeStyles,
  Theme,
  createStyles,
  IconButton,
} from "@material-ui/core";
import NoteIcon from "@material-ui/icons/Note";
import * as React from "react";
import { getCharacterIcon } from "../../../RulesTable/RulesTable";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    button: {
      margin: theme.spacing(1),
    },
  })
);

interface ChooseGuessProps {
  readonly playedCards: number[];
  readonly guess: number;
  readonly disabled?: boolean;
  chooseGuess(guess: number): void;
}

export const ChooseGuess: React.FunctionComponent<ChooseGuessProps> = (
  props: ChooseGuessProps
) => {
  const classes = useStyles();
  const { playedCards, guess, chooseGuess, disabled } = props;
  return (
    <Badge
      badgeContent={playedCards[guess - 1]}
      color={"secondary"}
      overlap="circle"
      anchorOrigin={{
        vertical: "bottom",
        horizontal: "right",
      }}
    >
      <IconButton
        size={"small"}
        color={"primary"}
        disabled={disabled || playedCards[guess - 1] === 0}
        onClick={() => chooseGuess(guess)}
        className={classes.button}
      >
        {getCharacterIcon(guess) || <NoteIcon />}
      </IconButton>
    </Badge>
  );
};
