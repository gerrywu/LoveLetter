import * as React from "react";
import { shallow } from "enzyme";
import { ChooseGuess } from "./ChooseGuess";

describe("ChooseGuess", () => {
  it("Matches snapshot", () => {
    const wrapper = shallow(
      <ChooseGuess playedCards={[]} guess={6} chooseGuess={jest.fn()} />
    );

    expect(wrapper.html()).toMatchSnapshot();
  });
});
