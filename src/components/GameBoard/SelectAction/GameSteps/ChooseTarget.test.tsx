import * as React from "react";
import { shallow } from "enzyme";
import { ChooseTarget } from "./ChooseTarget";

describe("ChooseTarget", () => {
  it("Matches snapshot", () => {
    const wrapper = shallow(
      <ChooseTarget
        target={1}
        playerTargetable={true}
        chooseTarget={jest.fn()}
      />
    );

    expect(wrapper.html()).toMatchSnapshot();
  });
});
