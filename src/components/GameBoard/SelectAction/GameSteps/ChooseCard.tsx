import Tooltip from "@material-ui/core/Tooltip";
import NoteIcon from "@material-ui/icons/Note";
import * as React from "react";
import { createStyles, Theme, Button } from "@material-ui/core";
import makeStyles from "@material-ui/core/styles/makeStyles";
import { cardNames, tooltips } from "../../../../interfaces/Constants";
import { getCharacterIcon } from "../../../RulesTable/RulesTable";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    button: {
      margin: theme.spacing(1),
      marginRight: 8,
    },
  })
);

interface ChooseCardProps {
  readonly holdingCard: number;
  readonly isPlayersTurn: boolean;
  readonly disabled?: boolean;
  chooseCard(cardId: number): void;
}

export const ChooseCard: React.FunctionComponent<ChooseCardProps> = (
  props: ChooseCardProps
) => {
  const classes = useStyles();
  const { holdingCard, isPlayersTurn, chooseCard, disabled } = props;
  const handleClick = () => {
    if (isPlayersTurn) {
      return chooseCard(holdingCard);
    }
  };
  return (
    <Tooltip title={tooltips[holdingCard - 1]}>
      <span>
        <Button
          size={"small"}
          variant={"outlined"}
          disabled={disabled}
          onClick={handleClick}
          className={classes.button}
          color={isPlayersTurn ? "primary" : "default"}
          startIcon={getCharacterIcon(holdingCard) || <NoteIcon />}
        >
          {cardNames[holdingCard - 1]}
        </Button>
      </span>
    </Tooltip>
  );
};
