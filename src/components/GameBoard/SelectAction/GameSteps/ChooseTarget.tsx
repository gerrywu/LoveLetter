import FaceIcon from "@material-ui/icons/Face";
import React from "react";
import makeStyles from "@material-ui/core/styles/makeStyles";
import { createStyles, Theme, Button } from "@material-ui/core";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    button: {
      margin: theme.spacing(1),
      marginRight: 8,
    },
  })
);

interface ChooseTargetProps {
  readonly target: number;
  readonly playerTargetable: boolean;
  chooseTarget(target: number): void;
}

export const ChooseTarget: React.FunctionComponent<ChooseTargetProps> = (
  props: ChooseTargetProps
) => {
  const classes = useStyles();
  const { target, playerTargetable, chooseTarget } = props;

  return (
    <Button
      size={"small"}
      variant={"outlined"}
      onClick={() => {
        if (playerTargetable) {
          return chooseTarget(target);
        }
      }}
      disabled={!playerTargetable}
      className={classes.button}
      color={playerTargetable ? "primary" : "default"}
      startIcon={<FaceIcon />}
    >{`P ${target}`}</Button>
  );
};
