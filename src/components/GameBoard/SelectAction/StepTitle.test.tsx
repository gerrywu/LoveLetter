import * as React from "react";
import { shallow } from "enzyme";
import { StepTitle } from "./StepTitle";

describe("StepTitle", () => {
  it("Matches snapshot", () => {
    const wrapper = shallow(<StepTitle state={{ cardId: 1, target: 3 }} />);

    expect(wrapper.html()).toMatchSnapshot();
  });
});
