import React from "react";
import { Step, Stepper } from "@material-ui/core";
import StepLabel, {
  StepLabelProps,
} from "@material-ui/core/StepLabel/StepLabel";
import Typography from "@material-ui/core/Typography/Typography";
import { getActiveStep } from "./SelectAction";
import { SelectActionState } from "../../../interfaces/SelectAction";

interface StepTitleProps {
  readonly state: SelectActionState;
}

export const StepTitle: React.FunctionComponent<StepTitleProps> = (
  props: StepTitleProps
) => {
  const { state } = props;
  const activeStep = getActiveStep(state);
  const steps = ["Card", "Target", "Guess"];

  return (
    <Stepper activeStep={activeStep} style={{ padding: 8 }}>
      {steps.map((label, index) => {
        const props = {};
        const labelProps: StepLabelProps = {};
        if (index === 2) {
          labelProps.optional = (
            <Typography variant="caption">Optional</Typography>
          );
        }
        return (
          <Step key={label} {...props}>
            <StepLabel {...labelProps}>{label}</StepLabel>
          </Step>
        );
      })}
    </Stepper>
  );
};
