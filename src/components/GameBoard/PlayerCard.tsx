import React from "react";
import { ListItem } from "@material-ui/core";
import { PlayedCardListItem } from "./PlayedCardListItem";
import { Player } from "../../interfaces/Game";

interface PlayerCardProps {
  readonly player: Player;
  readonly compact?: boolean;
}

export const PlayerCard: React.FC<PlayerCardProps> = ({ player, compact }) => {
  const { playedCards } = player;

  if (playedCards.length === 0) {
    return <ListItem style={{ minHeight: 48 }} />;
  }

  if (compact && playedCards.length > 0) {
    return (
      <PlayedCardListItem
        playedCard={playedCards[playedCards.length - 1]}
        playerProtected={player.protected}
      />
    );
  }

  return (
    <>
      {playedCards.map((playedCard, index) => (
        <PlayedCardListItem
          key={index}
          playedCard={playedCard}
          loading={false}
        />
      ))}
    </>
  );
};
