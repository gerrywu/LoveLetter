import * as React from "react";
import { shallow } from "enzyme";
import { PlayedCardListItem } from "./PlayedCardListItem";

describe("PlayedCardListItem", () => {
  it("Matches snapshot", () => {
    const wrapper = shallow(
      <PlayedCardListItem
        playedCard={{ cardId: 3, discarded: false, target: 1 }}
      />
    );

    expect(wrapper.html()).toMatchSnapshot();
  });
});
