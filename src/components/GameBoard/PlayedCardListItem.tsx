import React from "react";
import RemoveCircleIcon from "@material-ui/icons/RemoveCircle";
import PlayArrowIcon from "@material-ui/icons/PlayArrow";
import {
  Badge,
  CircularProgress,
  createStyles,
  ListItem,
  ListItemIcon,
  ListItemText,
} from "@material-ui/core";
import { PlayedCard } from "../../interfaces/Game";
import { getCharacterIcon } from "../RulesTable/RulesTable";
import { cardNames } from "../../interfaces/Constants";
import makeStyles from "@material-ui/core/styles/makeStyles";

const useStyles = makeStyles(() =>
  createStyles({
    root: {
      paddingLeft: 0,
      paddingRight: 0,
      marginLeft: 16,
      marginRight: 16,
      borderRadius: 10,
      width: "auto",
    },
  })
);

interface PlayedCardListItemProps {
  readonly playedCard: PlayedCard;
  readonly loading?: boolean;
  readonly playerProtected?: boolean;
}

export const PlayedCardListItem: React.FunctionComponent<PlayedCardListItemProps> = ({
  playedCard,
  loading,
  playerProtected,
}) => {
  const classes = useStyles();
  const icons: React.ReactElement[] = [];

  if (playedCard.discarded) {
    icons.push(<RemoveCircleIcon key={"discardedCard"} />);
  } else {
    icons.push(<PlayArrowIcon key={"playedCard"} />);
  }

  icons.push(
    <Badge badgeContent={playedCard.target} color="primary" key={"card"}>
      {getCharacterIcon(playedCard.cardId)}
    </Badge>
  );

  let secondary = "";
  if (playedCard.effect !== undefined) {
    secondary += playedCard.effect;
  } else {
    secondary += `${playedCard.discarded ? "Discarded" : "Played"} ${
      cardNames[playedCard.cardId - 1]
    }`;
  }

  let backgroundColor: string | undefined = undefined;
  if (playerProtected) {
    backgroundColor = "#c5e1a5";
  }

  return (
    <ListItem
      className={classes.root}
      style={{
        backgroundColor: backgroundColor,
        transition: "background-color 1s",
        transitionTimingFunction: "ease",
      }}
    >
      <ListItemIcon>{icons}</ListItemIcon>
      <ListItemText inset={true} primary={secondary} />
      {loading && <CircularProgress size={30} />}
    </ListItem>
  );
};
