import Paper from "@material-ui/core/Paper/Paper";
import React from "react";
import makeStyles from "@material-ui/core/styles/makeStyles";
import { createStyles, Theme } from "@material-ui/core";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      padding: theme.spacing(2),
      maxWidth: 800,
      margin: "auto",
    },
  })
);

export const TabContainer: React.FunctionComponent = (props) => {
  const classes = useStyles();
  return (
    <Paper className={classes.root} elevation={1}>
      {props.children}
    </Paper>
  );
};
