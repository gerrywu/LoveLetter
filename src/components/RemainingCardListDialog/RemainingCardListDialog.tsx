import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
} from "@material-ui/core";
import React from "react";
import { useSelector } from "react-redux";
import { cardEmojis, cardNames } from "../../interfaces/Constants";
import { getRemainingCardCountsFromPlayers } from "../../GameEngine/util";
import { AppState } from "../../GameEngine/reducers/rootReducer";

interface ReduxState {
  readonly remainingCardCounts: number[];
}

interface RemainingCardListDialogProps {
  readonly open: boolean;

  onClose(): void;
}

export const RemainingCardListDialog: React.FunctionComponent<RemainingCardListDialogProps> = (
  props: RemainingCardListDialogProps
) => {
  const { open, onClose } = props;
  const { remainingCardCounts } = useSelector<AppState, ReduxState>((state) => {
    return {
      remainingCardCounts: getRemainingCardCountsFromPlayers(
        state.gameReducer.players,
        { includePlayer: 1 }
      ),
    };
  });

  return (
    <Dialog
      open={open}
      onClose={onClose}
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description"
    >
      <DialogTitle id="alert-dialog-title">Remaining Cards</DialogTitle>
      <DialogContent>
        <Table>
          <TableHead>
            <TableRow>
              <TableCell>Rank</TableCell>
              <TableCell>Name</TableCell>
              <TableCell>Amount</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {remainingCardCounts.map((count, index: number) => (
              <TableRow key={index} hover>
                <TableCell component="th" scope="row">
                  {index + 1}
                </TableCell>
                <TableCell>
                  {cardEmojis[index]} {cardNames[index]}
                </TableCell>
                <TableCell>{count}</TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </DialogContent>
      <DialogActions>
        <Button onClick={onClose} color={"primary"} autoFocus={true}>
          Close
        </Button>
      </DialogActions>
    </Dialog>
  );
};
