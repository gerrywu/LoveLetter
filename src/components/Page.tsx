import React, { useState } from "react";
import { TabContainer } from "./TabContainer";
import { store } from "../App";
import makeStyles from "@material-ui/core/styles/makeStyles";
import { createStyles } from "@material-ui/core";
import { BottomNavigationBar } from "./BottomNavigationBar";
import { GameEndDialog } from "./GameEndDialog";
import { MessageSnackbar } from "./MessageSnackbar";
import { TabRouter } from "./TabRouter";
import { GameStartDialog } from "./GameStartDialog";
import { GameService } from "../GameEngine/GameService";
import { GameServiceImpl } from "../GameEngine/GameServiceImpl";

const useStyles = makeStyles(() =>
  createStyles({
    root: {
      flexGrow: 1,
      width: "100%",
    },
  })
);

export enum Tab {
  game = "game",
  rules = "rules",
  settings = "settings",
  fullGameBoard = "fullGameBoard",
  evaluation = "evaluation",
}

export const Page: React.FunctionComponent = () => {
  const classes = useStyles();
  const game: GameService = new GameServiceImpl(4, store);
  const [alertOpen, setAlertOpen] = useState<boolean>(false);
  const [alertMessage, setAlertMessage] = useState<string>("");
  const [gameEndDialogOpen, setGameEndDialogOpen] = useState<boolean>(false);
  const [gameStartDialogOpen, setGameStartDialogOpen] = useState<boolean>(true);
  const [tab, setTab] = useState<Tab>(Tab.game);

  const alert = (message: string) => {
    setAlertOpen(true);
    setAlertMessage(message);
  };

  const restart = () => {
    game.start();
    alert("Game restarts");
  };

  const handleTabChange = (event: React.ChangeEvent<any>, tab: string) => {
    setTab(tab as Tab);
  };

  const handleAlertClose = (
    event: React.SyntheticEvent | React.MouseEvent,
    reason?: string
  ) => {
    if (reason === "clickaway") {
      return;
    }

    setAlertOpen(false);
    setAlertMessage("");
  };

  const gameStartDialogOnClose = () => {
    setGameStartDialogOpen(false);
    game.start();
  };

  const gameEndDialogOnClose = (restartGame?: boolean) => {
    setGameEndDialogOpen(false);
    if (restartGame) {
      restart();
    }
  };

  return (
    <div className={classes.root}>
      <MessageSnackbar
        open={alertOpen}
        message={alertMessage}
        onClose={handleAlertClose}
      />

      <GameStartDialog
        open={gameStartDialogOpen}
        onClose={gameStartDialogOnClose}
      />

      <GameEndDialog
        winnerId={game.getWinnerId() || 0}
        dialogOpen={gameEndDialogOpen}
        onClose={gameEndDialogOnClose}
      />

      <TabContainer>
        <TabRouter
          tab={tab}
          game={game}
          restart={restart}
          setGameEndDialogOpen={setGameEndDialogOpen}
        />
      </TabContainer>

      <div style={{ marginBottom: 56 }} />
      <BottomNavigationBar tab={tab} handleChange={handleTabChange} />
    </div>
  );
};
