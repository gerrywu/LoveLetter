import * as React from "react";
import { shallow } from "enzyme";
import { TabContainer } from "./TabContainer";

describe("TabContainer", () => {
  it("Matches snapshot", () => {
    const wrapper = shallow(
      <TabContainer>
        <React.Fragment>Hello Jest!</React.Fragment>
      </TabContainer>
    );
    expect(wrapper.html()).toMatchSnapshot();
  });
});
