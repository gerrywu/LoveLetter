import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
} from "@material-ui/core";
import React from "react";

interface GameStartDialogProps {
  readonly open: boolean;
  onClose(): void;
}

export const GameStartDialog: React.FunctionComponent<GameStartDialogProps> = ({
  open,
  onClose,
}) => (
  <Dialog
    open={open}
    onClose={onClose}
    aria-labelledby="alert-dialog-title"
    aria-describedby="alert-dialog-description"
  >
    <DialogTitle id="alert-dialog-title">Welcome!</DialogTitle>
    <DialogContent>
      <DialogContentText id="alert-dialog-description">
        This is a replication of the board game Love Letter. You will be playing
        as player 1 against 3 other AI players.
        <br />
        If you need a refresher on how to play this game, please go to the rules
        tab.
      </DialogContentText>
    </DialogContent>
    <DialogActions>
      <Button onClick={onClose} color="primary">
        Start
      </Button>
      <Button onClick={onClose}>Do not show anymore</Button>
    </DialogActions>
  </Dialog>
);
