import React, { ReactElement } from "react";
import { Tab } from "./Page";
import { FullGameBoardTab } from "../tabs/FullGameBoardTab";
import { RulesTab } from "../tabs/RulesTab";
import { GameTab } from "../tabs/GameTab";
import { SettingsTab } from "../tabs/SettingsTab";
import { GameService } from "../GameEngine/GameService";

interface TabRouterProps {
  readonly tab: Tab;
  readonly game: GameService;

  restart(): void;

  setGameEndDialogOpen(open: boolean): void;
}

export const TabRouter: React.FC<TabRouterProps> = ({
  tab,
  game,
  restart,
  setGameEndDialogOpen,
}) => {
  let tabComponent: ReactElement;
  switch (tab) {
    case Tab.game: {
      tabComponent = (
        <GameTab
          playCard={game.playCard}
          nextTurn={() =>
            game.nextTurn(() => {
              setGameEndDialogOpen(true);
            })
          }
          restart={restart}
          alert={alert}
        />
      );
      break;
    }
    case Tab.fullGameBoard: {
      tabComponent = <FullGameBoardTab />;
      break;
    }
    case Tab.rules: {
      tabComponent = <RulesTab />;
      break;
    }
    case Tab.settings: {
      tabComponent = (
        <SettingsTab
          clearHistory={game.clearHistory}
          setNumberOfPlayers={game.setNumberOfPlayers}
        />
      );
      break;
    }
    default: {
      tabComponent = <></>;
      break;
    }
  }
  return tabComponent;
};
