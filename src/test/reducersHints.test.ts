import { initializeStateWithPlayer } from "../GameEngine/util";
import update from "immutability-helper";
import { GameReducer } from "../GameEngine/reducers/reducerGame";
import { Operator } from "../interfaces/Game";

describe("Hints", () => {
  // Invalidates
  it("Invalidates for next player", () => {
    const initialState = initializeStateWithPlayer(4);
    const state = update(initialState, {
      currentPlayerId: {
        $set: 1,
      },
      players: {
        [0]: {
          holdingCards: {
            $push: [1, 2],
          },
        },
        [1]: {
          holdingCards: {
            $push: [1],
          },
        },
        [2]: {
          holdingCards: {
            $push: [1],
          },
        },
      },
    });
    let nextState = GameReducer(state, {
      type: "PLAY_CARD",
      cardToPlay: { cardId: 1, target: 2, guess: 8 },
    });

    expect(nextState.hintsByPlayerId.get(2)).toBeDefined();

    nextState = GameReducer(state, {
      type: "NEXT_PLAYER",
    });

    expect(nextState.hintsByPlayerId.get(2)).toBeUndefined();
  });

  it("Guard correct guess", () => {
    const initialState = initializeStateWithPlayer(4);
    const state = update(initialState, {
      currentPlayerId: {
        $set: 2,
      },
      players: {
        [0]: {
          holdingCards: {
            $push: [8],
          },
        },
        [1]: {
          holdingCards: {
            $push: [1, 7],
          },
        },
      },
    });
    const nextState = GameReducer(state, {
      type: "PLAY_CARD",
      cardToPlay: { cardId: 1, target: 1, guess: 8 },
    });

    expect(nextState.hintsByPlayerId.get(1)).toBeDefined();
    const hints = nextState.hintsByPlayerId.get(1);
    expect(hints).toHaveLength(1);
    expect(hints).toBeDefined();
    if (hints) {
      expect(hints[0].cardId).toEqual(8);
      expect(hints[0].operator).toEqual(Operator.equal);
    }
  });

  it("Guard failed guess", () => {
    const initialState = initializeStateWithPlayer(4);
    const state = update(initialState, {
      currentPlayerId: {
        $set: 2,
      },
      players: {
        [0]: {
          holdingCards: {
            $push: [8],
          },
        },
        [1]: {
          holdingCards: {
            $push: [1, 7],
          },
        },
      },
    });
    const nextState = GameReducer(state, {
      type: "PLAY_CARD",
      cardToPlay: { cardId: 1, target: 1, guess: 7 },
    });

    expect(nextState.hintsByPlayerId.get(1)).toBeDefined();
    const hints = nextState.hintsByPlayerId.get(1);
    expect(hints).toHaveLength(1);
    expect(hints).toBeDefined();
    if (hints) {
      expect(hints[0].cardId).toEqual(7);
      expect(hints[0].operator).toEqual(Operator.notEqual);
    }
  });

  it("Baron wins", () => {
    const initialState = initializeStateWithPlayer(4);
    const state = update(initialState, {
      currentPlayerId: {
        $set: 2,
      },
      players: {
        [0]: {
          holdingCards: {
            $push: [7],
          },
        },
        [1]: {
          holdingCards: {
            $push: [3, 8],
          },
        },
      },
    });
    const nextState = GameReducer(state, {
      type: "PLAY_CARD",
      cardToPlay: { cardId: 3, target: 1 },
    });

    expect(nextState.hintsByPlayerId.get(2)).toBeDefined();
    const hints = nextState.hintsByPlayerId.get(2);
    expect(hints).toHaveLength(1);
    expect(hints).toBeDefined();
    if (hints) {
      expect(hints[0].cardId).toEqual(7);
      expect(hints[0].operator).toEqual(Operator.larger);
    }
  });

  it("Baron loses", () => {
    const initialState = initializeStateWithPlayer(4);
    const state = update(initialState, {
      currentPlayerId: {
        $set: 2,
      },
      players: {
        [0]: {
          holdingCards: {
            $push: [8],
          },
        },
        [1]: {
          holdingCards: {
            $push: [3, 7],
          },
        },
      },
    });
    const nextState = GameReducer(state, {
      type: "PLAY_CARD",
      cardToPlay: { cardId: 3, target: 1 },
    });

    expect(nextState.hintsByPlayerId.get(1)).toBeDefined();
    const hints = nextState.hintsByPlayerId.get(1);
    expect(hints).toHaveLength(1);
    expect(hints).toBeDefined();
    if (hints) {
      expect(hints[0].cardId).toEqual(7);
      expect(hints[0].operator).toEqual(Operator.larger);
    }
  });
});
