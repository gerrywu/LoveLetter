import { GameReducer } from "../GameEngine/reducers/reducerGame";
import {
  getAvailableCardSize,
  initializeStateWithPlayer,
} from "../GameEngine/util";
import update from "immutability-helper";

describe("General Reducer", () => {
  it("shouldn't mutate the original state", () => {
    const state = {
      GameReducer: 0,
    };
    const nextState = Object.assign({}, state);
    nextState.GameReducer = 1;
    expect(state.GameReducer).toEqual(0);
  });

  it("should draw a card from the availableCards and append to the current player's holding card", () => {
    const state = initializeStateWithPlayer(4);
    const nextState = GameReducer(state, { type: "DRAW_CARD" });
    expect(getAvailableCardSize(nextState.availableCards)).toEqual(
      getAvailableCardSize(state.availableCards) - 1
    );

    const nextStateHoldingCardLength =
      nextState.players[state.currentPlayerId - 1].holdingCards.length;
    const previousStateHoldingCardLength =
      state.players[state.currentPlayerId - 1].holdingCards.length;
    expect(nextStateHoldingCardLength).toEqual(
      previousStateHoldingCardLength + 1
    );
  });

  it("should setup the game when RESTART action is dispatched", () => {
    const state = GameReducer(undefined, { type: "RESTART" });
    expect(getAvailableCardSize(state.availableCards)).toEqual(16 - 5);

    expect(state.players[0].holdingCards.length).toEqual(1);
    expect(state.players[1].holdingCards.length).toEqual(1);
    expect(state.players[2].holdingCards.length).toEqual(1);
    expect(state.players[3].holdingCards.length).toEqual(1);
  });

  it.skip("should discard the played card", () => {});
  it("should skip players turn if they are dead", () => {
    const initialState = initializeStateWithPlayer(4);
    const state = update(initialState, {
      currentPlayerId: {
        $set: 4,
      },
      players: {
        [0]: {
          dead: {
            $set: true,
          },
        },
        [3]: {
          holdingCards: {
            $push: [4],
          },
        },
      },
    });
    let nextState = GameReducer(state, {
      type: "PLAY_CARD",
      cardToPlay: {
        cardId: state.players[3].holdingCards[0],
        target: -1,
        guess: -1,
      },
    });

    nextState = GameReducer(nextState, {
      type: "NEXT_PLAYER",
    });

    expect(nextState.currentPlayerId).toEqual(2);
  });
  it.skip("should end the game when there are no cards left", () => {});

  it("should decide the right winner when the game ends", () => {
    const initialState = initializeStateWithPlayer(4);
    const state = update(initialState, {
      currentPlayerId: {
        $set: 4,
      },
      availableCards: {
        $set: {
          Guard: 0,
          Priest: 0,
          Baron: 0,
          Handmaid: 0,
          Prince: 0,
          King: 0,
          Countess: 0,
          Princess: 0,
        },
      },
      players: {
        [0]: {
          holdingCards: {
            $push: [1],
          },
        },
        [1]: {
          holdingCards: {
            $push: [2],
          },
        },
        [2]: {
          holdingCards: {
            $push: [3],
          },
        },
        [3]: {
          holdingCards: {
            $push: [4, 7],
          },
        },
      },
    });

    const nextState = GameReducer(state, {
      type: "PLAY_CARD",
      cardToPlay: {
        cardId: state.players[3].holdingCards[0],
        target: 2,
      },
    });
    expect(nextState.gameEnds.winner).not.toEqual(undefined);
    expect(nextState.gameEnds.winner?.id).toEqual(4);
  });

  it("should throw warning when target is invalid", () => {
    const initialState = initializeStateWithPlayer(3);
    const state = update(initialState, {
      currentPlayerId: {
        $set: 3,
      },
      players: {
        [0]: {
          holdingCards: {
            $push: [1],
          },
        },
        [1]: {
          holdingCards: {
            $push: [2],
          },
        },
        [2]: {
          holdingCards: {
            $push: [1],
          },
        },
      },
    });
    expect(() =>
      GameReducer(state, {
        type: "PLAY_CARD",
        cardToPlay: {
          cardId: state.players[2].holdingCards[0],
          target: 4,
          guess: 5,
        },
      })
    ).toThrowError(TypeError);
  });
});
