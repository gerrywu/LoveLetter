import { GameReducer } from "../GameEngine/reducers/reducerGame";
import { initializeStateWithPlayer } from "../GameEngine/util";
import update from "immutability-helper";

describe("Card Resolution", () => {
  it.skip("[General] should not be able to play a card against a dead player", () => {});
  it.skip("[General] should not be able to play a card against a protected player", () => {});

  it("[Guard] should die when a player guessed the right card", () => {
    const initialState = initializeStateWithPlayer(4);
    const state = update(initialState, {
      players: {
        [0]: {
          holdingCards: {
            $push: [1, 3],
          },
        },
        [1]: {
          holdingCards: {
            $push: [8],
          },
        },
      },
    });
    const nextState = GameReducer(state, {
      type: "PLAY_CARD",
      cardToPlay: { cardId: 1, target: 2, guess: 8 },
    });

    expect(nextState.players[0].dead).toEqual(false);
    expect(nextState.players[1].dead).toEqual(true);
  });

  it("[Guard] should have card effetcs", () => {
    const initialState = initializeStateWithPlayer(4);
    const state = update(initialState, {
      players: {
        [0]: {
          holdingCards: {
            $push: [1, 3],
          },
        },
        [1]: {
          holdingCards: {
            $push: [8],
          },
        },
      },
    });
    const nextState = GameReducer(state, {
      type: "PLAY_CARD",
      cardToPlay: { cardId: 1, target: 2, guess: 8 },
    });

    expect(nextState.players[0].playedCards.length).toEqual(1);
    expect(nextState.players[0].playedCards[0].effect).toBeDefined();
  });

  it("[Priest] should add a players hand to seen cards if played Priest against them", () => {
    const initialState = initializeStateWithPlayer(4);
    const state = update(initialState, {
      players: {
        [0]: {
          holdingCards: {
            $push: [2, 3],
          },
        },
        [1]: {
          holdingCards: {
            $push: [8],
          },
        },
      },
    });
    const nextState = GameReducer(state, {
      type: "PLAY_CARD",
      cardToPlay: { cardId: 2, target: 2, guess: -1 },
    });

    expect(Array.isArray(nextState.players[0].seenCards)).toBe(true);
    expect(nextState.players[0].seenCards.length).toEqual(1);
    expect(nextState.players[0].seenCards[0]).toEqual({
      cardId: 8,
      playerId: 2,
    });
  });

  it("[Baron] should compare cards and set the lower rank loser dead", () => {
    const initialState = initializeStateWithPlayer(4);
    const state = update(initialState, {
      players: {
        [0]: {
          holdingCards: {
            $push: [1, 3],
          },
        },
        [1]: {
          holdingCards: {
            $push: [8],
          },
        },
      },
    });
    const nextState = GameReducer(state, {
      type: "PLAY_CARD",
      cardToPlay: { cardId: 3, target: 2, guess: -1 },
    });

    expect(nextState.players[0].dead).toEqual(true);
    expect(nextState.players[1].dead).toEqual(false);
  });
  it.skip("[Handmaid] should set current player as protected", () => {});
  it("[Prince] should make a player discard a card", () => {
    const initialState = initializeStateWithPlayer(4);
    const state = update(initialState, {
      players: {
        [0]: {
          holdingCards: {
            $push: [5, 1],
          },
        },
        [1]: {
          holdingCards: {
            $push: [2],
          },
        },
      },
    });
    const nextState = GameReducer(state, {
      type: "PLAY_CARD",
      cardToPlay: { cardId: 5, target: 2, guess: -1 },
    });

    expect(Array.isArray(nextState.players[1].playedCards)).toBe(true);
    expect(nextState.players[1].playedCards.length).toEqual(1);
    expect(nextState.players[1].playedCards[0]).toEqual({
      cardId: 2,
      discarded: true,
      guess: undefined,
      target: undefined,
    });
  });
  it("[Prince] should draw the first card of the game if there are no cards left in the deck", () => {
    const initialState = initializeStateWithPlayer(4);
    const state = update(initialState, {
      players: {
        [0]: {
          holdingCards: {
            $push: [5],
          },
        },
        [1]: {
          holdingCards: {
            $push: [1],
          },
        },
      },
      availableCards: {
        $set: {},
      },
      firstCard: {
        $set: 3,
      },
    });

    const nextState = GameReducer(state, {
      type: "PLAY_CARD",
      cardToPlay: { cardId: 5, target: 2, guess: -1 },
    });
    expect(nextState.players[1].holdingCards.length).toEqual(1);
  });
  it("[Prince] should not draw the another card if player is dead", () => {
    const initialState = initializeStateWithPlayer(4);
    const state = update(initialState, {
      players: {
        [0]: {
          holdingCards: {
            $push: [5],
          },
        },
        [1]: {
          holdingCards: {
            $push: [8],
          },
        },
      },
      availableCards: {
        $set: {},
      },
      firstCard: {
        $set: 3,
      },
    });
    const nextState = GameReducer(state, {
      type: "PLAY_CARD",
      cardToPlay: { cardId: 5, target: 2, guess: -1 },
    });
    expect(nextState.players[1].dead).toEqual(true);
    expect(nextState.players[1].holdingCards.length).toEqual(0);
  });
  it("[King] should swap hands with another player", () => {
    const initialState = initializeStateWithPlayer(4);
    const state = update(initialState, {
      players: {
        [0]: {
          holdingCards: {
            $push: [3, 6],
          },
        },
        [1]: {
          holdingCards: {
            $push: [8],
          },
        },
      },
    });

    const nextState = GameReducer(state, {
      type: "PLAY_CARD",
      cardToPlay: { cardId: 6, target: 2, guess: -1 },
    });

    expect(Array.isArray(nextState.players[0].holdingCards)).toBe(true);
    expect(nextState.players[0].holdingCards.length).toEqual(1);
    expect(nextState.players[0].holdingCards[0]).toEqual(8);

    expect(Array.isArray(nextState.players[1].holdingCards)).toBe(true);
    expect(nextState.players[1].holdingCards.length).toEqual(1);
    expect(nextState.players[1].holdingCards[0]).toEqual(3);
  });
  it.skip("[Countess] should check if the other card isn't Prince or King", () => {});
  it("[Princess] should die when a player plays Princess", () => {
    const initialState = initializeStateWithPlayer(4);
    const state = update(initialState, {
      players: {
        [0]: {
          holdingCards: {
            $push: [8],
          },
        },
      },
    });
    const nextState = GameReducer(state, {
      type: "PLAY_CARD",
      cardToPlay: { cardId: 8, target: -1, guess: -1 },
    });

    expect(nextState.players[0].dead).toEqual(true);
  });

  it("[Princess] player should die when being forced to discard princess with prince", () => {
    const initialState = initializeStateWithPlayer(4);
    const state = update(initialState, {
      players: {
        [0]: {
          holdingCards: {
            $push: [5],
          },
        },
        [1]: {
          holdingCards: {
            $push: [8],
          },
        },
      },
    });
    const nextState = GameReducer(state, {
      type: "PLAY_CARD",
      cardToPlay: { cardId: 5, target: 2, guess: -1 },
    });

    expect(Array.isArray(nextState.players[1].playedCards)).toBe(true);
    expect(nextState.players[1].playedCards.length).toEqual(1);
    expect(nextState.players[1].playedCards[0]).toEqual({
      cardId: 8,
      discarded: true,
      guess: undefined,
      target: undefined,
    });
    expect(nextState.players[1].dead).toEqual(true);
  });
});
