import RandomAI from "../GameEngine/AI/randomAI";
import { initializeStateWithPlayer } from "../GameEngine/util";
import { Player } from "../interfaces/Game";

describe("RandomAI", () => {
  it("prioritize playing handmaid", () => {
    const state = Object.assign({}, initializeStateWithPlayer(4), {
      currentPlayerId: 2,
    });
    state.players[1].playedCards.push({
      cardId: 3,
    });
    state.players[1].dead = false;
    state.players[1].holdingCards.push(4);
    state.players[1].holdingCards.push(8);

    const randomAI = new RandomAI();
    const card = randomAI.getAction(state);

    expect(card.cardId).toBe(4);
    expect(card.target).toBeUndefined;
    expect(card.guess).toBeUndefined;
  });

  it("should be able to identify target", () => {
    const players = [
      {
        id: 1,
        dead: false,
        protected: false,
        holdingCards: [3, 7],
        playedCards: [
          {
            cardId: 1,
            target: 2,
            guess: 5,
            effect: "🛡️ Player 2 doesn't have 🤴",
          },
          {
            cardId: 1,
            target: 2,
            guess: 8,
            effect: "🗡️ Player 2 has ❄️",
          },
        ],
        seenCards: [],
      },
      {
        id: 2,
        dead: true,
        protected: false,
        holdingCards: [8],
        playedCards: [
          {
            cardId: 2,
            target: 3,
            effect: "👀 Player 3",
          },
        ],
        seenCards: [
          {
            cardId: 3,
            playerId: 3,
          },
        ],
      },
      {
        id: 3,
        dead: true,
        protected: false,
        holdingCards: [5],
        playedCards: [
          {
            cardId: 4,
            effect: "❤️ Protected",
          },
          {
            cardId: 3,
            target: 1,
            effect: "⚰️ Lost to 1 with Baron",
          },
        ],
        seenCards: [],
      },
      {
        id: 4,
        dead: false,
        protected: false,
        holdingCards: [1, 2],
        playedCards: [
          {
            cardId: 1,
            target: 1,
            guess: 8,
            effect: "🛡️ Player 1 doesn't have ❄️",
          },
        ],
        seenCards: [],
      },
    ];

    const state = Object.assign({}, initializeStateWithPlayer(4), {
      players,
      currentPlayerId: 4,
    });

    const randomAI = new RandomAI();
    const card = randomAI.getAction(state);
    expect(card.cardId).toEqual(1);
    expect(card.guess).toEqual(7);
    expect(card.target).toBeDefined();
  });

  it("isGuessValid", () => {
    const players: Player[] = [
      {
        id: 1,
        dead: false,
        protected: false,
        holdingCards: [6],
        playedCards: [
          {
            cardId: 2,
            target: 2,
            effect: "👀 Player 2 has Guard",
          },
          {
            cardId: 1,
            target: 3,
            guess: 8,
            effect: "🛡️ Player 3 doesn't have ❄️",
          },
        ],
        seenCards: [
          {
            cardId: 1,
            playerId: 2,
          },
        ],
      },
      {
        id: 2,
        dead: false,
        protected: false,
        holdingCards: [1, 7],
        playedCards: [
          {
            cardId: 1,
            target: 1,
            guess: 8,
            effect: "🛡️ Player 1 doesn't have ❄️",
          },
        ],
        seenCards: [],
      },
      {
        id: 3,
        dead: false,
        protected: false,
        holdingCards: [5],
        playedCards: [
          {
            cardId: 1,
            target: 4,
            guess: 8,
            effect: "🛡️ Player 4 doesn't have ❄️",
          },
        ],
        seenCards: [],
      },
      {
        id: 4,
        dead: false,
        protected: false,
        holdingCards: [5],
        playedCards: [
          {
            cardId: 1,
            target: 2,
            guess: 8,
            effect: "🛡️ Player 2 doesn't have ❄️",
          },
        ],
        seenCards: [],
      },
    ];

    const state = Object.assign({}, initializeStateWithPlayer(4), {
      players,
      currentPlayerId: 2,
    });

    const randomAI = new RandomAI();
    const card = randomAI.getAction(state);
    expect(card.cardId).toEqual(1);
    expect(card.guess).toEqual(8);
    expect(card.target).toBeDefined();
  });
});
