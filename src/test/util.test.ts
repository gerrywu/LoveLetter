import { discardCard } from "../GameEngine/util";
import { Player } from "../interfaces/Game";

describe("Util Functions", () => {
  it("should discard existing holding card", () => {
    const currentPlayerId = 1;
    const players: ReadonlyArray<Player> = [
      {
        id: 1,
        holdingCards: [3],
        seenCards: [],
        playedCards: [],
        dead: false,
        protected: false,
      },
      {
        id: 2,
        holdingCards: [4],
        seenCards: [],
        playedCards: [],
        dead: false,
        protected: false,
      },
      {
        id: 3,
        holdingCards: [5],
        seenCards: [],
        playedCards: [],
        dead: false,
        protected: false,
      },
      {
        id: 4,
        holdingCards: [6],
        seenCards: [],
        playedCards: [],
        dead: false,
        protected: false,
      },
    ];

    const nextPlayers = discardCard(players, currentPlayerId, { cardId: 3 });
    expect(nextPlayers[currentPlayerId - 1].holdingCards.length).toEqual(0);
  });
});
