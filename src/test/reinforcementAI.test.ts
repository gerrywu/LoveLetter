import math from "mathjs";
import ReinforcementAI from "../GameEngine/AI/reinforcementAI";
import { initializeStateWithPlayer } from "../GameEngine/util";
import update from "immutability-helper";

describe("ReinforcementAI.public functions", () => {
  it("ReinforcementAI.initialize", () => {
    const reinforcementAI = new ReinforcementAI([2, 9, 8, 8], [8, 4, 7]);
    reinforcementAI.initialize();
    expect(reinforcementAI.QValueTable.length).toEqual(
      reinforcementAI.SASizeArr[0]
    );
    expect(reinforcementAI.QValueTable[0].length).toEqual(
      reinforcementAI.SASizeArr[1]
    );
    expect(reinforcementAI.QValueTable[0][0].length).toEqual(
      reinforcementAI.SASizeArr[2]
    );
  });

  it("ReinforcementAI.getBestAction", () => {
    const state = initializeStateWithPlayer(4);
    state.players[1].playedCards.push({
      cardId: 3,
    });
    state.players[1].dead = false;
    state.players[1].holdingCards.push(4);
    state.players[1].holdingCards.push(8);

    const reinforcementAI = new ReinforcementAI([2, 9, 8, 8], [8, 4, 7]);
    reinforcementAI.initialize();

    const card = reinforcementAI.getAction(state);
    expect([4, 8]).toContain(card.cardId);
    expect([1]).toContain(card.target);
    expect([2, 3, 4, 5, 6, 7, 8]).toContain(card.guess);
  });

  it("ReinforcementAI.learn", () => {
    const initialState = initializeStateWithPlayer(4);
    const state = update(initialState, {
      players: {
        [1]: {
          playedCards: {
            $push: [{ cardId: 3 }],
          },
          holdingCards: {
            $set: [1, 1],
          },
          dead: {
            $set: false,
          },
        },
      },
    });

    const reinforcementAI = new ReinforcementAI([2, 9, 8, 8], [8, 4, 7]);
    reinforcementAI.initialize();

    const card = reinforcementAI.getAction(state);
    expect(card.cardId).toEqual(1);
    expect([2, 3, 4]).toContain(card.target);
    expect([2, 3, 4, 5, 6, 7, 8]).toContain(card.guess);

    reinforcementAI.learn(state);
  });
});

describe("ReinforcementAI.private functions", () => {
  it("SObjectToSVector", () => {
    const reinforcementAI = new ReinforcementAI([2, 9, 8, 8], [8, 4, 7]);
    const SVector = reinforcementAI.SObjectToSVector({
      player0dead: true,
      player0lastCardId: 7,
      player0holdingCard0: 6,
      player0holdingCard1: 1,
    });

    expect(SVector).toEqual([1, 6, 5, 0]);
  });

  it("generateActionVectors", () => {
    const reinforcementAI = new ReinforcementAI([2, 9, 8, 8], [8, 4, 7]);
    // let SVector = [1, 6, 5, 0];
    const player0holdingCard0 = 8;
    const target = [2, 3, 4];
    const guess = [2, 3, 4, 5, 6, 7, 8];
    const AVectors = reinforcementAI.generateActionVectors(
      player0holdingCard0,
      target,
      guess
    );
    expect(AVectors.length).toEqual(target.length * guess.length);
    expect(AVectors[0]).toEqual([
      player0holdingCard0 - 1,
      target[0] - 1,
      guess[0] - 2,
    ]);
    expect(AVectors[1]).toEqual([
      player0holdingCard0 - 1,
      target[0] - 1,
      guess[1] - 2,
    ]);
  });

  it("AVectorToActionObject", () => {
    const reinforcementAI = new ReinforcementAI([2, 9, 8, 8], [8, 4, 7]);
    // guess [2, 3, 4, 5, 6, 7, 8]
    // action vec [0, 1, 2, 3, 4, 5, 6]
    const actionObject = reinforcementAI.AVectorToActionObject([0, 2, 6]);
    expect(actionObject.cardId).toEqual(1);
    expect(actionObject.target).toEqual(3);
    expect(actionObject.guess).toEqual(8);
  });

  it("getMaxQValueSAVectorGivenSAVectors", () => {
    const reinforcementAI = new ReinforcementAI([2, 9, 8, 8], [8, 4, 7]);
    reinforcementAI.initialize();
    const SAVectors = [
      [0, 0, 0, 0, 0, 2, 5],
      [0, 0, 0, 0, 0, 2, 6],
    ];
    const SAVector = reinforcementAI.getMaxQValueSAVectorGivenSAVectors(
      SAVectors
    );
    expect(SAVectors).toContain(SAVector);
  });

  it("getQValue", () => {
    const reinforcementAI = new ReinforcementAI([2, 9, 8, 8], [8, 4, 7]);
    reinforcementAI.initialize();
    const qValue = reinforcementAI.getQValue([1, 8, 7, 7, 7, 3, 6]);
    expect(qValue).toBeLessThan(1);
    expect(qValue).toBeGreaterThan(0);
  });

  it("combineSAVectors", () => {
    const SVector = [1, 6, 5, 1];
    const AVectors = [
      [2, 3, 4],
      [1, 2, 3],
    ];
    const reinforcementAI = new ReinforcementAI([2, 9, 8, 8], [8, 4, 7]);
    expect(reinforcementAI.combineSAVectors(SVector, AVectors)).toEqual([
      [1, 6, 5, 1, 2, 3, 4],
      [1, 6, 5, 1, 1, 2, 3],
    ]);
  });

  it("SAVectorToActionObject", () => {
    const reinforcementAI = new ReinforcementAI([2, 9, 8, 8], [8, 4, 7]);
    expect(
      reinforcementAI.SAVectorToActionObject([0, 0, 0, 0, 0, 2, 5])
    ).toEqual({
      cardId: 1,
      target: 3,
      guess: 7,
    });
  });
});

describe("ReinforcementAI.utility functions", () => {
  it("mathjs.subset", () => {
    const b = [
      [0, 1],
      [2, 3],
    ];
    expect(math.subset(b, math.index(1, 0))).toEqual(2);
    const reinforcementAI = new ReinforcementAI([2, 9, 8, 8], [8, 4, 7]);
    reinforcementAI.initialize();
    const SAVectors = [
      [0, 0, 0, 0, 0, 2, 6],
      [0, 0, 0, 0, 0, 2, 7],
    ];
    // There's got to be a better way of doing this...
    expect(
      math.subset(
        reinforcementAI.QValueTable,
        reinforcementAI.convertSAVectorToMathIndex(SAVectors[0])
      )
    ).toBeLessThan(1);
    expect(
      math.subset(
        reinforcementAI.QValueTable,
        reinforcementAI.convertSAVectorToMathIndex(SAVectors[0])
      )
    ).toBeGreaterThan(0);
  });
});
