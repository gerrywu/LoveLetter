import {
  getHighestNotYetAppearedCard,
  getNonDeadNonProtectedPlayers,
  getRemainingCardCountsFromPlayers,
} from "../util";
import { GameState, Operator, PlayedCard } from "../../interfaces/Game";
import { PlayerInterface } from "../../interfaces/Player";

export default class RandomAI implements PlayerInterface {
  public getAction(gameState: GameState): PlayedCard {
    const players = gameState.players;
    const playerId = gameState.currentPlayerId;
    const hintsByPlayerId = gameState.hintsByPlayerId;

    const currentPlayer = players[playerId - 1];
    const { seenCards } = currentPlayer;
    const holdingCards = currentPlayer.holdingCards.sort();
    console.assert(holdingCards.length === 2);
    const cardId = holdingCards[0];

    // Follow the Countess rule.
    if (holdingCards.includes(7)) {
      if (holdingCards.includes(5) || holdingCards.includes(6)) {
        return {
          cardId: 7,
        };
      }
    }

    let guess;
    let target = (playerId % players.length) + 1;
    const targetablePlayerList = getNonDeadNonProtectedPlayers(
      playerId,
      players
    );
    if (cardId === 1) {
      // Randomly choose from the highest not yet appeared card.

      const cardRemainingCountsFromPlayers = getRemainingCardCountsFromPlayers(
        players
      );
      guess = getHighestNotYetAppearedCard(cardRemainingCountsFromPlayers);
    }

    // Use seen cards
    if (seenCards.length !== 0) {
      // Use the last one
      const seenCard = seenCards[seenCards.length - 1];
      if (targetablePlayerList.includes(seenCard.playerId)) {
        if (
          !players[seenCard.playerId - 1].playedCards.find(
            (playedCard) => playedCard.cardId === seenCard.cardId
          )
        ) {
          if (cardId === 1) {
            return {
              cardId,
              target: seenCard.playerId,
              guess: seenCard.cardId,
            };
          } else if (cardId === 3 && holdingCards[1] > seenCard.cardId) {
            return {
              cardId,
              target: seenCard.playerId,
            };
          } else if (cardId === 5 && seenCard.cardId > 3) {
            return {
              cardId,
              target: seenCard.playerId,
            };
          }
        }
      }
    }

    // Prioritize on playing handmaid.
    if (holdingCards.includes(4)) {
      return { cardId: 4 };
    }

    if (targetablePlayerList.length === 0) {
      // The player is the winner.
    } else {
      // Use hints to filter out target player
      if (
        hintsByPlayerId?.size !== 0 &&
        hintsByPlayerId?.keys() !== undefined
      ) {
        for (const hintPlayerId of hintsByPlayerId?.keys()) {
          const hints = hintsByPlayerId.get(hintPlayerId);
          if (!hints) {
            break;
          }
          for (const hint of hints) {
            if (hint.cardId === guess && hint.operator === Operator.notEqual) {
              const index = targetablePlayerList.findIndex(
                (playerId) => playerId === hintPlayerId
              );
              targetablePlayerList.splice(index, 1);
              break;
            }
          }
        }
      }

      // Randomly select one player to play the card against.
      const randomPlayerIndex = Math.floor(
        Math.random() * targetablePlayerList.length
      );
      target = targetablePlayerList[randomPlayerIndex];
    }
    return { cardId, target, guess };
  }
}
