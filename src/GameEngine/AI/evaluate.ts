import ReinforcementAI from "./reinforcementAI";
import { GameServiceImpl } from "../GameServiceImpl";

export default class Evaluation {
  private game: GameServiceImpl;
  constructor() {
    this.game = new GameServiceImpl(4, undefined);
    const reinforcementAI = new ReinforcementAI([2, 9, 8, 8], [8, 4, 7]);
    reinforcementAI.initialize();
    this.game.setPlayer(2, reinforcementAI);
  }

  public start = () => {
    return new Promise(() => {
      // do a thing, possibly async, then…
      // const winRate = [0, 0, 0, 0];
      // const games = 2;
      //
      // let p: Promise<number> = new Promise<number>((resolve) => {
      //   return 0;
      // });
      // Wrap this.game.play into a Promise and do asynchronous calls.
      // for (let i = 0; i < games; i++) {
      //   p = p
      //     .then(
      //       (winnerId): Promise<number> => {
      //         console.log(winnerId);
      //         // @ts-ignore
      //         winRate[winnerId - 1]++;
      //
      //         // console.log(winRate);
      //         if (i === games - 1) {
      //           return this.game
      //             .play()
      //             .then((winnerId) => {
      //               winRate[winnerId - 1]++;
      //
      //               resolve({
      //                 winRate: [
      //                   (winRate[0] / games) * 100,
      //                   (winRate[1] / games) * 100,
      //                   (winRate[2] / games) * 100,
      //                   (winRate[3] / games) * 100,
      //                 ],
      //               });
      //               return 0;
      //             })
      //             .catch((err) => {
      //               console.log(err);
      //             });
      //         }
      //         return this.game.play();
      //       }
      //     )
      //     .catch((err) => {
      //       console.log(err);
      //     });
      // }
    });
  };
}
