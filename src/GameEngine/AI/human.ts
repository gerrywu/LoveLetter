import { PlayedCard } from "../../interfaces/Game";
import { PlayerInterface } from "../../interfaces/Player";

export default class HumanPlayer implements PlayerInterface {
  getAction(): PlayedCard {
    return { cardId: -1 };
  }
}
