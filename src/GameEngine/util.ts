import update from "immutability-helper";
import {
  cardNames,
  cardRank,
  getInitialState,
  initialPlayerState,
} from "../interfaces/Constants";
import {
  Dictionary,
  GameState,
  PlayedCard,
  Player,
  SeenCard,
} from "../interfaces/Game";

export const getRemainingCardCountsFromPlayers = (
  players: ReadonlyArray<Player>,
  options?: { includePlayer: number }
): number[] => {
  // Given players.holding cards, return array of the remaining count of the cards.
  const remainingCardCounts = [5, 2, 2, 2, 2, 1, 1, 1];
  for (const player of players) {
    for (const playedCard of player.playedCards) {
      remainingCardCounts[playedCard.cardId - 1]--;
    }
    if (player.dead) {
      remainingCardCounts[player.holdingCards[0] - 1]--;
    } else if (options?.includePlayer === player.id) {
      remainingCardCounts[player.holdingCards[0] - 1]--;
      remainingCardCounts[player.holdingCards[1] - 1]--;
    }
  }
  return remainingCardCounts;
};

export const getLivingPlayerSize = (players: ReadonlyArray<Player>) => {
  return players.filter((player) => !player.dead).length;
};

export const calculateWinner = (players: ReadonlyArray<Player>) => {
  let winnerId = -1;
  players
    .filter((player) => !player.dead)
    .forEach((player) => {
      if (
        winnerId === -1 ||
        players[winnerId - 1].holdingCards[0] < player.holdingCards[0]
      ) {
        winnerId = player.id;
      }
    });

  return winnerId;
};

export const getNonDeadNonProtectedPlayers = (
  playerId: number,
  players: ReadonlyArray<Player>
): number[] => {
  const nonDeadNonProtectedPlayerList: number[] = [];
  players.forEach((player) => {
    if (player.id !== playerId && !player.protected && !player.dead) {
      nonDeadNonProtectedPlayerList.push(player.id);
    }
  });
  return nonDeadNonProtectedPlayerList;
};

export const getAvailableCardSize = (
  availableCards: Dictionary<number>
): number => {
  let totalCards = 0;
  for (const key in availableCards) {
    if (availableCards.hasOwnProperty.call(availableCards, key)) {
      totalCards += availableCards[key];
    }
  }
  return totalCards;
};

export const checkGameEnd = (
  players: ReadonlyArray<Player>,
  availableCards: Dictionary<number>
) => {
  if (
    getAvailableCardSize(availableCards) <= 0 ||
    getLivingPlayerSize(players) <= 1
  ) {
    const winner = calculateWinner(players);
    return { gameEnd: true, winner };
  }
  return { gameEnd: false, winner: -1 };
};

export const getHighestNotYetAppearedCard = (
  cardsNotPlayedYet: number[]
): number => {
  for (let index = 7; index > 0; index--) {
    if (cardsNotPlayedYet[index] !== 0) {
      return index + 1;
    }
  }

  return 2;
};

// This function will return a random card index based on the availableCards passed in.
export const getRandomCard = (availableCards: Dictionary<number>): number => {
  // Get the number of total cards
  const totalCards = getAvailableCardSize(availableCards);

  if (totalCards === 0) {
    return -1;
  }

  const randomCardNumber = Math.floor(Math.random() * totalCards);

  let temp = 0;
  let drawedCard = "Guard";
  for (const key in availableCards) {
    if (availableCards.hasOwnProperty.call(availableCards, key)) {
      temp += availableCards[key];
      if (temp > randomCardNumber) {
        drawedCard = key;
        break;
      }
    }
  }

  return cardRank[drawedCard];
};

export const nextPlayerId = (
  players: ReadonlyArray<Player>,
  currentPlayerId: number
): number => {
  // Next non dead player
  const totalPlayers = players.length;
  let nextPlayerIndex = currentPlayerId % totalPlayers;

  while (players[nextPlayerIndex].dead) {
    nextPlayerIndex = (nextPlayerIndex + 1) % totalPlayers;
  }

  return players[nextPlayerIndex].id;
};

export const resetProtection = (
  players: ReadonlyArray<Player>,
  currentPlayerId: number
): ReadonlyArray<Player> =>
  update(players, {
    [currentPlayerId - 1]: {
      protected: {
        $set: false,
      },
    },
  });

export const discardCard = (
  players: ReadonlyArray<Player>,
  playerId: number,
  discardCard: PlayedCard
): ReadonlyArray<Player> => {
  const player: Player = players[playerId - 1];
  const index = player.holdingCards.findIndex(
    (cardId) => cardId === discardCard.cardId
  );
  if (index === -1) {
    throw Error(
      `Can't find card ${discardCard.cardId} in player ${playerId}'s hand.`
    );
  }

  return update(players, {
    [playerId - 1]: {
      holdingCards: {
        $splice: [[index, 1]],
      },
    },
  });
};

/* Push to array */

export const addPlayedCard = (
  players: ReadonlyArray<Player>,
  playerId: number,
  card: PlayedCard
): ReadonlyArray<Player> => {
  return update(players, {
    [playerId - 1]: {
      playedCards: {
        $push: [card],
      },
    },
  });
};

export const addHoldingCards = (
  players: ReadonlyArray<Player>,
  playerId: number,
  card: number
): ReadonlyArray<Player> =>
  update(players, {
    [playerId - 1]: {
      holdingCards: {
        $push: [card],
      },
    },
  });

export const addSeenCards = (
  players: ReadonlyArray<Player>,
  playerId: number,
  seenCard: SeenCard
) => {
  return update(players, {
    [playerId - 1]: {
      seenCards: {
        $push: [seenCard],
      },
    },
  });
};

export const setPlayerDead = (
  players: ReadonlyArray<Player>,
  playerId: number
): ReadonlyArray<Player> => {
  let nextPlayers: ReadonlyArray<Player> = addHoldingCards(
    players,
    playerId,
    players[playerId - 1].holdingCards[0]
  );
  nextPlayers = discardCard(nextPlayers, playerId, {
    cardId: nextPlayers[playerId - 1].holdingCards[0],
  });
  return update(nextPlayers, {
    [playerId - 1]: {
      dead: {
        $set: true,
      },
    },
  });
};

export const drawCardForPlayer = (
  previousState: GameState,
  playerId: number
) => {
  const randomCardId = getRandomCard(previousState.availableCards) || 1;
  // Remove the cardDrew from availableCards
  const arr = Object.assign({}, previousState.availableCards);
  if (arr[cardNames[randomCardId - 1]] !== undefined) {
    arr[cardNames[randomCardId - 1]]--;
  }

  return Object.assign({}, previousState, {
    players: addHoldingCards(previousState.players, playerId, randomCardId),
    availableCards: arr,
  });
};

export const drawCard = (previousState: GameState) =>
  drawCardForPlayer(previousState, previousState.currentPlayerId);

export const initializeStateWithPlayer = (
  numberOfPlayers: number
): GameState => {
  let newState = Object.assign({}, getInitialState(numberOfPlayers));

  // Add players
  for (let player = 0; player < numberOfPlayers; player += 1) {
    const newPlayerState = Object.assign({}, initialPlayerState, {
      id: player + 1,
    });

    newState = update(newState, {
      players: {
        [player]: {
          $set: newPlayerState,
        },
      },
    });
  }

  return newState;
};
