import { combineReducers, createStore, Store } from "redux";
import {
  drawCard,
  nextPlayer,
  playCard,
  restart,
  setNumberOfPlayers,
} from "./actions/game";

import ReinforcementAI from "./AI/reinforcementAI";
import RandomAI from "./AI/randomAI";
import HumanPlayer from "./AI/human";
import { GameReducer } from "./reducers/reducerGame";
import { HistoryReducer } from "./reducers/reducerHistory";
import { clearHistory, updateHistory } from "./actions/history";
import { PlayedCard } from "../interfaces/Game";
import { AppState } from "./reducers/rootReducer";
import { PlayerInterface } from "../interfaces/Player";
import { GameService } from "./GameService";

// This class abstracts the game play, acting as the dealer of the game.
// This should be the only interface the UI should know about.
// Redux is being used internally.
export class GameServiceImpl implements GameService {
  private store: Store<AppState>;
  private readonly players: PlayerInterface[];

  constructor(players: number, store?: Store) {
    this.players = Array(players);
    if (store !== null && store !== undefined) {
      this.store = store;
    } else {
      this.store = createStore(
        combineReducers({
          gameReducer: GameReducer,
          historyReducer: HistoryReducer,
        })
      );
    }

    // Player 1
    this.setPlayer(1, new HumanPlayer());
    // Player 2
    const reinforcementAI = new ReinforcementAI([2, 9, 8, 8], [8, 4, 7]);
    reinforcementAI.initialize();
    this.setPlayer(2, new RandomAI());
    // Player 3
    this.setPlayer(3, new RandomAI());
    // Player 4
    this.setPlayer(4, new RandomAI());
  }

  // Public functions
  public setPlayer(playerId: number, player: PlayerInterface) {
    this.players[playerId - 1] = player;
  }

  public getWinnerId() {
    return this.store.getState().gameReducer.gameEnds?.winner?.id;
  }

  public clearHistory = () => {
    this.store.dispatch(clearHistory());
  };

  public setNumberOfPlayers = (numberOfPLayers: number) => {
    this.store.dispatch(setNumberOfPlayers(numberOfPLayers));
    this.store.dispatch(restart());
  };

  public start() {
    this.store.dispatch(restart());
    this.drawCard();
    // this.nextTurn();
  }

  public playCard = (playedCard: PlayedCard) => {
    this.store.dispatch(playCard(playedCard));
  };

  private drawCard = (): void => {
    const { gameReducer } = this.store.getState();
    this.store.dispatch(drawCard(gameReducer.currentPlayerId));
  };

  public nextTurn = (resolve?: (value?: any | PromiseLike<any>) => void) => {
    // Setup next player
    this.store.dispatch(nextPlayer());

    if (this.store.getState().gameReducer.gameEnds.winner !== undefined) {
      // Game end
      const winnerId = this.getWinnerId() || -1;
      console.log(`Game Ends, winner is ${winnerId}`);
      this.store.dispatch(updateHistory(winnerId));
      if (resolve) {
        resolve(this.getWinnerId());
      }
    } else if (
      this.store.getState().gameReducer.players[
        this.store.getState().gameReducer.currentPlayerId - 1
      ].dead
    ) {
      // Skip dead players
      this.nextTurn(resolve);
    } else {
      this.drawCard();
      const currentPlayer = this.players[
        this.store.getState().gameReducer.currentPlayerId - 1
      ];
      switch (currentPlayer.constructor) {
        case HumanPlayer:
          // Wait until the human response, let UI call this.nextTurn() directly.
          break;
        case ReinforcementAI: {
          (currentPlayer as ReinforcementAI).learn(
            this.store.getState().gameReducer
          );

          const reinforcementAICard = currentPlayer.getAction(
            this.store.getState().gameReducer
          );
          this.playCard(reinforcementAICard);
          return;
        }
        case RandomAI: {
          // Random AI move
          const randomAICard: PlayedCard = currentPlayer.getAction(
            this.store.getState().gameReducer
          );
          this.playCard(randomAICard);
          return;
        }
        default:
          break;
      }
    }
  };
}
