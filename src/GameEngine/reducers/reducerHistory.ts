import { CLEAR_HISTORY, UPDATE_HISTORY } from "../actions/history";
import { HistoryAction, HistoryState } from "../../interfaces/Game";
import { Reducer } from "redux";

export const HistoryReducer: Reducer<HistoryState, HistoryAction> = (
  state,
  action
) => {
  if (typeof state === "undefined") {
    let totalGames = "0";
    let totalWins = "0";
    if (
      (localStorage.getItem("totalGames") === "NaN" &&
        localStorage.getItem("totalWins") === "NaN") ||
      (localStorage.getItem("totalGames") === null &&
        localStorage.getItem("totalWins") === null)
    ) {
      localStorage.setItem("totalGames", totalGames);
      localStorage.setItem("totalWins", totalWins);
    } else {
      totalGames = localStorage.getItem("totalGames") || "0";
      totalWins = localStorage.getItem("totalWins") || "0";
    }
    return {
      totalGames,
      totalWins,
    };
  }

  switch (action.type) {
    case UPDATE_HISTORY: {
      const totalGames = parseInt(state.totalGames, 10) + 1;
      const totalWins =
        parseInt(state.totalWins, 10) + (action.winnerId === 1 ? 1 : 0);
      localStorage.setItem("totalGames", String(totalGames));
      localStorage.setItem("totalWins", String(totalWins));
      return Object.assign(
        {},
        {
          totalGames: localStorage.getItem("totalGames") || "0",
          totalWins: localStorage.getItem("totalWins") || "0",
        }
      );
    }
    case CLEAR_HISTORY: {
      const totalGames = "0";
      const totalWins = "0";
      localStorage.setItem("totalGames", totalGames);
      localStorage.setItem("totalWins", totalWins);
      return {
        totalGames,
        totalWins,
      };
    }
    default: {
      return state;
    }
  }
};
