import update from "immutability-helper";
import {
  DISCARD_CARD,
  DRAW_CARD,
  NEXT_PLAYER,
  PLAY_CARD,
  RESTART,
  SET_NUMBER_OF_PLAYERS,
} from "../actions/game";
import { cardEmojis, cardNames } from "../../interfaces/Constants";
import {
  addHoldingCards,
  addPlayedCard,
  addSeenCards,
  calculateWinner,
  discardCard,
  drawCard,
  drawCardForPlayer,
  getAvailableCardSize,
  getLivingPlayerSize,
  getRandomCard,
  initializeStateWithPlayer,
  nextPlayerId,
  resetProtection,
  setPlayerDead,
} from "../util";
import {
  Dictionary,
  GameAction,
  GameState,
  Hint,
  Operator,
  PlayedCard,
  Player,
} from "../../interfaces/Game";
import { Reducer } from "redux";

const checkPlayable = (state: GameState, playerId: number, cardId: number) => {
  if ([4, 7, 8].includes(cardId)) {
    return true;
  }

  const player = state.players[playerId - 1];
  return !player.dead && !player.protected;
};

const addHints = (
  hintsByPlayerId: Map<number, Hint[]>,
  playerId: number,
  cardId: number,
  operator: Operator
): void => {
  if (hintsByPlayerId.get(playerId) === undefined) {
    hintsByPlayerId.set(playerId, []);
  }
  const hints = hintsByPlayerId.get(playerId);
  hints?.push({
    cardId,
    operator,
  });
};

const checkGameEnd = (
  players: ReadonlyArray<Player>,
  availableCards: Dictionary<number>
) => {
  if (
    getAvailableCardSize(availableCards) <= 0 ||
    getLivingPlayerSize(players) <= 1
  ) {
    const winnerId = calculateWinner(players);
    return { gameEnd: true, winnerId };
  }
  return { gameEnd: false, winnerId: -1 };
};

const resolve = (state: GameState, cardToPlay: PlayedCard): GameState => {
  if (!checkPlayable(state, cardToPlay.target || 1, cardToPlay.cardId)) {
    return state;
  }

  const nextState = Object.assign({}, state);

  // 4, 7 and 8 doesn't have to have target
  if ([4, 7, 8].includes(cardToPlay.cardId)) {
    switch (cardToPlay.cardId) {
      case 4: {
        cardToPlay.effect = `❤️ Protected`;
        return update(state, {
          players: {
            [state.currentPlayerId - 1]: {
              protected: {
                $set: true,
              },
            },
          },
        });
      }
      case 8: {
        return update(nextState, {
          players: {
            $set: setPlayerDead(state.players, state.currentPlayerId),
          },
        });
      }
      default:
        return state;
    }
  }

  if (cardToPlay.target === undefined) {
    return state;
  }

  switch (cardToPlay.cardId) {
    case 1:
      if (cardToPlay.guess === undefined) {
        return state;
      }

      if (
        cardToPlay.guess ===
        state.players[cardToPlay.target - 1].holdingCards[0]
      ) {
        cardToPlay.effect = `🗡️ Player ${cardToPlay.target} has ${
          cardToPlay.guess && cardEmojis[cardToPlay.guess - 1]
        }`;

        // Update hint
        addHints(
          state.hintsByPlayerId,
          cardToPlay.target,
          cardToPlay.guess,
          Operator.equal
        );

        return update(state, {
          players: {
            $set: setPlayerDead(state.players, cardToPlay.target),
          },
        });
      } else {
        cardToPlay.effect = `🛡️ Player ${cardToPlay.target} doesn't have ${
          cardToPlay.guess && cardEmojis[cardToPlay.guess - 1]
        }`;

        addHints(
          state.hintsByPlayerId,
          cardToPlay.target,
          cardToPlay.guess,
          Operator.notEqual
        );

        return update(state, {
          players: {
            [state.currentPlayerId - 1]: {
              playedCards: {
                [state.players[state.currentPlayerId - 1].playedCards.length -
                1]: {
                  effect: {
                    $set: cardToPlay.effect,
                  },
                },
              },
            },
          },
        });
      }
    case 2:
      if (state.currentPlayerId === 1) {
        cardToPlay.effect = `👀 Player ${cardToPlay.target} has ${
          cardNames[
            nextState.players[cardToPlay.target - 1].holdingCards[0] - 1
          ]
        }`;
      } else {
        cardToPlay.effect = `👀 Player ${cardToPlay.target}`;
      }
      return update(state, {
        players: {
          $set: addSeenCards(nextState.players, nextState.currentPlayerId, {
            cardId: nextState.players[cardToPlay.target - 1].holdingCards[0],
            playerId: cardToPlay.target,
          }),
        },
      });

    case 3: {
      const cardValue1 =
        state.players[state.currentPlayerId - 1].holdingCards[0];
      const cardValue2 = state.players[cardToPlay.target - 1].holdingCards[0];
      if (cardValue1 > cardValue2) {
        cardToPlay.effect = `🗡️ Knocked out ${cardToPlay.target} (${
          cardEmojis[
            nextState.players[cardToPlay.target - 1].holdingCards[0] - 1
          ]
        }) with Baron`;

        addHints(
          state.hintsByPlayerId,
          state.currentPlayerId,
          cardValue2,
          Operator.larger
        );

        return update(nextState, {
          players: {
            $set: setPlayerDead(state.players, cardToPlay.target),
          },
        });
      } else if (cardValue1 < cardValue2) {
        cardToPlay.effect = `⚰️ Lost to ${cardToPlay.target} with Baron`;

        addHints(
          state.hintsByPlayerId,
          cardToPlay.target,
          cardValue1,
          Operator.larger
        );

        return update(nextState, {
          players: {
            $set: setPlayerDead(state.players, state.currentPlayerId),
          },
        });
      }
      cardToPlay.effect = `Ties with ${cardToPlay.target} (${
        cardEmojis[nextState.players[cardToPlay.target - 1].holdingCards[0] - 1]
      })`;
      break;
    }
    case 4:
      cardToPlay.effect = `❤️ Protected`;
      return update(state, {
        players: {
          [state.currentPlayerId - 1]: {
            protected: {
              $set: true,
            },
          },
        },
      });
    case 5: {
      // Prince, Discard and draw
      const cardToDiscard =
        state.players[cardToPlay.target - 1].holdingCards[0];
      const state1 = discardCard(state.players, cardToPlay.target, {
        cardId: cardToDiscard,
      });
      const state2 = addPlayedCard(state1, cardToPlay.target, {
        cardId: cardToDiscard,
        target: undefined,
        guess: undefined,
        discarded: true,
      });

      cardToPlay.effect = `Told player ${cardToPlay.target} to discard ${
        cardEmojis[cardToDiscard - 1]
      }`;

      if (cardToDiscard === 8) {
        return update(nextState, {
          players: {
            $set: setPlayerDead(state2, cardToPlay.target),
          },
        });
      } else if (getAvailableCardSize(state.availableCards) === 0) {
        // Give the hidden card to player
        return update(nextState, {
          players: {
            $set: addHoldingCards(
              state2,
              cardToPlay.target,
              nextState.firstCard
            ),
          },
        });
      } else {
        return update(nextState, {
          $set: drawCardForPlayer(
            update(nextState, {
              players: {
                $set: state2,
              },
            }),
            cardToPlay.target
          ),
        });
      }
    }
    case 6: {
      cardToPlay.effect = `👑 Exchanged cards with ${cardToPlay.target}`;

      // Add seen cards and holding cards
      return update(state, {
        players: {
          [state.currentPlayerId - 1]: {
            holdingCards: {
              $set: [state.players[cardToPlay.target - 1].holdingCards[0]],
            },
            seenCards: {
              $push: [
                {
                  cardId:
                    state.players[state.currentPlayerId - 1].holdingCards[0],
                  playerId: cardToPlay.target,
                },
              ],
            },
          },
          [cardToPlay.target - 1]: {
            holdingCards: {
              $set: [state.players[state.currentPlayerId - 1].holdingCards[0]],
            },
            seenCards: {
              $push: [
                {
                  cardId: state.players[cardToPlay.target - 1].holdingCards[0],
                  playerId: state.currentPlayerId,
                },
              ],
            },
          },
        },
      });
    }
    default:
      break;
  }

  return nextState;
};

export const GameReducer: Reducer<GameState, GameAction> = (state, action) => {
  if (typeof state === "undefined") {
    const numberOfPlayers = 4;

    let newState = initializeStateWithPlayer(numberOfPlayers);

    // Setup and draw cards.
    // Discard a card at the start of the game.
    const randomCardId = getRandomCard(newState.availableCards) || 1;
    newState = Object.assign({}, newState, {
      firstCard: randomCardId,
    });
    newState.availableCards[cardNames[randomCardId - 1]] -= 1;

    for (let player = 0; player < numberOfPlayers; player += 1) {
      newState = drawCardForPlayer(newState, player + 1);
    }

    return newState;
  }

  switch (action.type) {
    case RESTART: {
      const numberOfPlayers = state.numberOfPlayers || 4;

      let newState = initializeStateWithPlayer(numberOfPlayers);

      // Setup and draw cards.
      // Discard a card at the start of the game.
      const randomCardId = getRandomCard(newState.availableCards) || 1;
      newState = Object.assign({}, newState, {
        firstCard: randomCardId,
      });
      newState.availableCards[cardNames[randomCardId - 1]] -= 1;

      for (let player = 0; player < numberOfPlayers; player += 1) {
        newState = drawCardForPlayer(newState, player + 1);
      }

      return newState;
    }
    case SET_NUMBER_OF_PLAYERS: {
      return update(state, {
        numberOfPlayers: {
          $set: action.numberOfPlayers || 0,
        },
      });
    }
    case NEXT_PLAYER: {
      // Next Player
      const state2 = update(state, {
        currentPlayerId: {
          $set: nextPlayerId(state.players, state.currentPlayerId),
        },
      });

      // Invalidate hints
      const state3 = update(state2, {
        hintsByPlayerId: {
          $remove: [state2.currentPlayerId],
        },
      });

      // Reset protected
      return update(state3, {
        players: {
          $set: resetProtection(state3.players, state3.currentPlayerId),
        },
      });
    }

    case DRAW_CARD:
      return drawCard(state);

    case DISCARD_CARD: {
      if (!action.card) {
        return state;
      }

      return update(state, {
        players: {
          $set: discardCard(state.players, state.currentPlayerId, action.card),
        },
      });
    }
    case PLAY_CARD: {
      if (!action.cardToPlay) {
        return state;
      }

      // TODO: Move all these into other reducer functions and have the GameEngine act as dealer.

      // Remove holding cards
      const state1 = update(state, {
        players: {
          $set: discardCard(
            state.players,
            state.currentPlayerId,
            action.cardToPlay
          ),
        },
      });

      // Add played Card
      const state2 = update(state1, {
        players: {
          $set: addPlayedCard(
            state1.players,
            state1.currentPlayerId,
            action.cardToPlay
          ),
        },
      });

      // Resolve
      const state3 = update(state2, {
        $set: resolve(state2, action.cardToPlay),
      });

      // Check if game ends
      const gameEnds = checkGameEnd(state3.players, state3.availableCards);
      if (gameEnds.gameEnd) {
        return update(state3, {
          gameEnds: {
            winner: {
              $set: state3.players[gameEnds.winnerId - 1],
            },
          },
        });
      }

      return state3;
    }
  }
  return state;
};
