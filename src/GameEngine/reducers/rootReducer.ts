import { combineReducers } from "redux";
import { GameReducer } from "./reducerGame";
import { HistoryReducer } from "./reducerHistory";

export const rootReducer = combineReducers({
  gameReducer: GameReducer,
  historyReducer: HistoryReducer,
});

export type AppState = ReturnType<typeof rootReducer>;
