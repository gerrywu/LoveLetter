export const UPDATE_HISTORY = "UPDATE_HISTORY";

interface UpdateHistoryAction {
  type: typeof UPDATE_HISTORY;
  winnerId: number;
}

export const updateHistory = (winnerId: number): HistoryActionTypes => ({
  type: UPDATE_HISTORY,
  winnerId,
});

export const CLEAR_HISTORY = "CLEAR_HISTORY";

interface ClearHistoryAction {
  type: typeof CLEAR_HISTORY;
}

export const clearHistory = (): HistoryActionTypes => ({
  type: CLEAR_HISTORY,
});

export type HistoryActionTypes = UpdateHistoryAction | ClearHistoryAction;
