import { PlayedCard } from "../../interfaces/Game";

export const PLAY_CARD = "PLAY_CARD";

interface PlayCardAction {
  type: typeof PLAY_CARD;
  cardToPlay: PlayedCard;
}

export const playCard = (cardToPlay: PlayedCard): GameActionTypes => ({
  type: PLAY_CARD,
  cardToPlay,
});

export const DISCARD_CARD = "DISCARD_CARD";

interface DiscardCardAction {
  type: typeof DISCARD_CARD;
  card: PlayedCard;
}

export const discardCard = (card: PlayedCard): GameActionTypes => ({
  type: "DISCARD_CARD",
  card,
});

export const NEXT_PLAYER = "NEXT_PLAYER";

interface NextPlayerAction {
  type: typeof NEXT_PLAYER;
}

export const nextPlayer = (): GameActionTypes => ({
  type: NEXT_PLAYER,
});

export const DRAW_CARD = "DRAW_CARD";

interface DrawCardAction {
  type: typeof DRAW_CARD;
  playerId: number;
}

export const drawCard = (playerId: number): GameActionTypes => ({
  type: DRAW_CARD,
  playerId,
});

export const RESTART = "RESTART";

interface RestartAction {
  type: typeof RESTART;
}

export const restart = (): GameActionTypes => ({
  type: RESTART,
});

export const SET_NUMBER_OF_PLAYERS = "SET_NUMBER_OF_PLAYERS";

interface SetNumberOfPlayersAction {
  type: typeof SET_NUMBER_OF_PLAYERS;
  numberOfPlayers: number;
}

export const setNumberOfPlayers = (
  numberOfPlayers: number
): GameActionTypes => ({
  type: SET_NUMBER_OF_PLAYERS,
  numberOfPlayers,
});

export type GameActionTypes =
  | PlayCardAction
  | DiscardCardAction
  | NextPlayerAction
  | DrawCardAction
  | RestartAction
  | SetNumberOfPlayersAction;
