import { PlayedCard } from "../interfaces/Game";

export interface GameService {
  start(): void;

  getWinnerId(): number | undefined;

  clearHistory(): void;

  setNumberOfPlayers(numberOfPlayers: number): void;

  playCard(card: PlayedCard): void;

  nextTurn(resolve?: (value?: any | PromiseLike<any>) => void): void;
}
