import { Provider } from "react-redux";
import { Page } from "./components/Page";
import { createMuiTheme, MuiThemeProvider } from "@material-ui/core/styles";
import React from "react";
import { createStore } from "redux";
import { rootReducer } from "./GameEngine/reducers/rootReducer";

export const store = createStore(
  rootReducer,
  // @ts-ignore
  window.__REDUX_DEVTOOLS_EXTENSION__ &&
    // @ts-ignore
    window.__REDUX_DEVTOOLS_EXTENSION__({ serialize: true })
);

const theme = createMuiTheme({
  palette: {
    primary: {
      main: "#1976d2",
    },
  },
  overrides: {
    // Style sheet name ⚛️
    MuiListItemIcon: {
      // Name of the rule
      root: {
        // Some CSS
        color: "black",
      },
    },
    MuiCardContent: {
      root: {
        "&:last-child": {
          paddingBottom: 12,
        },
      },
    },
    MuiListItemText: {
      inset: {
        paddingLeft: 28,
      },
    },
  },
});

export const App: React.FunctionComponent = () => (
  <MuiThemeProvider theme={theme}>
    <Provider store={store}>
      <Page />
    </Provider>
  </MuiThemeProvider>
);
